#include "r_local.h"
#include "engine/common/library.h"
#include "beamdef.h"
#include "particledef.h"
#include "entity_types.h"

#include "tier1/convar.h"
#include "tier1/concommand.h"

Convar r_debug_draw_depth("r_debug_draw_depth", "0", 0, "Draws the depth texture to your screen");
Convar r_debug_draw_test("r_debug_draw_test", "0");

struct screenspace_state_t
{
	// Bloom shader
	IShader* bloom;

	// Flat rect that covers the full screen
	CMesh* screenMesh;

	// Generic params
	bool shouldRender = false;
	bool wasInitShaders = false;
};

static void R_InitScreenspaceShaders(screenspace_state_t* state);
static void SHD_DrawBloom(screenspace_state_t* state);

void R_DrawScreenSpaceEffects()
{
	static screenspace_state_t state;

	R_InitScreenspaceShaders(&state);

	if(state.shouldRender)
	{

		// Draw the bloom shader
		state.bloom->Draw(state.screenMesh);

	}

}


static void R_InitScreenspaceShaders(screenspace_state_t* state)
{
	if(state->wasInitShaders)
		return;
	state->wasInitShaders = true;

	state->bloom = GL_GetShaderByName("bloom");

	// Create a flat plane mesh to cover the screen
	state->screenMesh = new CMesh();
	CMeshBuilder meshBuilder(state->screenMesh, 128);
	meshBuilder.Begin(EMeshBuilderMode::TRIANGLES);
	{
		meshBuilder.Vertex2f(0, 0);
		meshBuilder.Advance();
		meshBuilder.Vertex2f(0.f, 1.f);
		meshBuilder.Advance();
		meshBuilder.Vertex2f(1.f, 0.f);
		meshBuilder.Advance();
	}
	meshBuilder.End();
	meshBuilder.WriteChanges();

	state->shouldRender = true;
}

static void SHD_DrawBloom(screenspace_state_t* state)
{

}
