
#include "r_ui.h"

class CRenderUI : public IRenderUIBackend
{
public:
	virtual bool	    Init() { return true; }
	virtual bool	    PreInit() { return true; }
	virtual void	    Shutdown() {}
	virtual const char* GetName() { return "CRenderUI001"; };
	virtual const char* GetParentInterface() { return "IRenderUI001"; };

	bool HasBackend(const char* backendName) override
	{
#ifdef HAVE_NUKLEAR
		if(Q_strcmp(backendName, RENDERUI_BACKEND_NUKLEAR) == 0)
			return true;
#endif
		return false;
	}

	/**
	 * Draws the UI with the specified private data.
	 * @param backendName Name of the backend to use
	 * @param privateData Private data. Make sure the struct and layout is the same for both caller and renderer
	 * @return true if everything went OK
	 */
	virtual bool Draw(const char* backendName, void* privateData)
	{
#ifdef HAVE_NUKLEAR
		if(Q_strcmp(backendName, RENDERUI_BACKEND_NUKLEAR) == 0)
			return UI_Nuklear_Draw(privateData);
#endif
		return false;
	}

	virtual uint64_t UploadTexture(const char* backend, const void* texData, int w, int h, EImageFormat fmt)
	{
#ifdef HAVE_NUKLEAR
		if(Q_strcmp(backend, RENDERUI_BACKEND_NUKLEAR) == 0)
		{
			return UI_Nuklear_UploadTexture(texData, w, h, fmt);
		}
#endif
		return 0;
	}
};

EXPOSE_INTERFACE(CRenderUI);