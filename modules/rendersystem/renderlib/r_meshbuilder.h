/*
 *
 * r_meshbuilder.h - Class for building meshes
 *
 */

#pragma once

#include <initializer_list>

namespace rendersystem
{

/* Defines the type of buffer */
enum class EBufferType
{
	HOST = 0, /* Host-only */
	SHARED,	  /* Mapped in both host and GPU memory, changes are propagated every so-often */
	CLIENT,	  /* GPU only */
};

/* Various supported types by CBufferFormat */
enum class EBufferFormatType : unsigned char
{
	VEC2F, // Float vectors
	VEC3F,
	VEC4F,
	VEC2D, // Double vectors
	VEC3D,
	VEC4D,
	I8, // Signed integers
	I16,
	I32,
	I64,
	U8, // Unsigned integers
	U16,
	U32,
	U64,
	F16, // Floats
	F32,
	F64,
	F80,
	MAT2, // 2x2 matricies
	MAT3,
	MAT4,
	OTHER, // If your type is not listed here, select other
};

template<int N>
class BufferFormat
{
private:

public:
	BufferFormat(std::initializer_list<EBufferFormatType> format);

	/* Offset in bytes of item at the X index */
	int Offset(int index);

	/* Type of the index */
	EBufferFormatType Type(int index);

	/* Stride between the index */
	int Stride(int index);

	/* Total number of components */
	int Components();

};

}
