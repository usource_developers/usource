
#include "../r_local.h"

#define BLOOM_SHADER_FS "shaders/post/bloom.fs.glsl"
#define BLOOM_SHADER_VS "shaders/post/bloom.vs.glsl"

class CBloomShader : public IShader
{
public:
	/* Attribs we need to hold */
	GLint mTransform;
	GLint vPositionAttrib;
	GLint vNormalAttrib;
	GLint vTexCoordAttrib;

	matrix4x4 TransformMatrix;


	CBloomShader() :
		IShader("bloom")
	{
		Matrix4x4_LoadIdentity(TransformMatrix);
	}

	void Init(bool reload) override
	{
		this->LoadFragment(BLOOM_SHADER_FS);
		this->LoadVertex(BLOOM_SHADER_VS);
	}

	void SetupAttribs() override
	{
		mTransform = GetUniformLocation("mTransform");
		vPositionAttrib = GetAttribLocation("vPosition");
		vNormalAttrib = GetAttribLocation("vNormal");
		vTexCoordAttrib = GetAttribLocation("vTexCoord");
	}

	/**
	 * @brief Called to draw the shader
	 */
	void Draw(CMesh* mesh) override
	{
		glUseProgram(m_program);

		// Create our transform matrix
		//int w, h;
		//gEngfuncs.CL_GetScreenInfo(&w, &h);
		//Matrix4x4_CreateOrtho(TransformMatrix, w, 0, h, 0, -1, 1);

		glEnableVertexAttribArray(vPositionAttrib);
		glEnableVertexAttribArray(vNormalAttrib);
		glEnableVertexAttribArray(vTexCoordAttrib);

		glBindBuffer(GL_ARRAY_BUFFER, mesh->m_vertexBuffer);

		size_t texOffset = sizeof(float) * 4;
		size_t normalOffset = sizeof(float) * 8;

		glVertexAttribPointer(vPositionAttrib, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(vTexCoordAttrib, 4, GL_FLOAT, GL_FALSE, 0, (GLvoid*)texOffset);
		glVertexAttribPointer(vNormalAttrib, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)normalOffset);

		glUniformMatrix4fv(mTransform, 1, false, (GLfloat*)TransformMatrix);

		glDrawArrays(GL_TRIANGLES, 0, mesh->m_vboSize / (sizeof(float) * 11));

		glDisableVertexAttribArray(vPositionAttrib);
		glDisableVertexAttribArray(vNormalAttrib);
		glDisableVertexAttribArray(vTexCoordAttrib);

		glUseProgram(0);
	}
};

REGISTER_SHADER(CBloomShader);
