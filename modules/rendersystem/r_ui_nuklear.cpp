#include "r_local.h"
#include "r_efx.h"
#include "event_flags.h"
#include "entity_types.h"
#include "triangleapi.h"
#include "customentity.h"
#include "engine/common/pm_local.h"

#ifdef HAVE_NUKLEAR
#define NK_IMPLEMENTATION
#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_IMPLEMENTATION
#include "nuklear.h"
#endif

struct ref_nk_vertex
{
	float position[2];
	float uv[2];
	byte col[4];
};


bool UI_Nuklear_Draw(void* privateData)
{

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	auto pPvt = (RenderUINuklearPvt_t*)privateData;

	nk_context* ctx = (nk_context*)pPvt->ctx;
	nk_buffer* cmdbuf = (nk_buffer*)pPvt->cmdbuf;
	nk_buffer* vbuf = (nk_buffer*)pPvt->vbuf;
	nk_buffer* ebuf = (nk_buffer*)pPvt->ebuf;

	const nk_draw_index *offset = NULL;
	GLsizei vs = sizeof(ref_nk_vertex);
	size_t vp = offsetof(ref_nk_vertex, position);
	size_t vt = offsetof(ref_nk_vertex, uv);
	size_t vc = offsetof(ref_nk_vertex, col);

	static const nk_draw_vertex_layout_element vertex_layout[] = {
		{NK_VERTEX_POSITION, NK_FORMAT_FLOAT, NK_OFFSETOF(ref_nk_vertex, position)},
		{NK_VERTEX_TEXCOORD, NK_FORMAT_FLOAT, NK_OFFSETOF(ref_nk_vertex, uv)},
		{NK_VERTEX_COLOR, NK_FORMAT_R8G8B8A8, NK_OFFSETOF(ref_nk_vertex, col)},
		{NK_VERTEX_LAYOUT_END}
	};

	nk_draw_null_texture nulltex;

	nk_convert_config config;
	config.vertex_layout = vertex_layout;
	config.vertex_size = sizeof(struct ref_nk_vertex);
	config.vertex_alignment = NK_ALIGNOF(ref_nk_vertex);
	config.null = nulltex;
	config.circle_segment_count = 32;
	config.curve_segment_count = 32;
	config.arc_segment_count = 32;
	config.global_alpha = 1.0f;
	config.shape_AA = NK_ANTI_ALIASING_OFF;
	config.line_AA = NK_ANTI_ALIASING_OFF;

	nk_convert(ctx, cmdbuf, vbuf, ebuf, &config);

	/* setup vertex buffer pointer */
	{const void *vertices = nk_buffer_memory_const(vbuf);
		glVertexPointer(2, GL_FLOAT, vs, (const void*)((const nk_byte*)vertices + vp));
		glTexCoordPointer(2, GL_FLOAT, vs, (const void*)((const nk_byte*)vertices + vt));
		glColorPointer(4, GL_UNSIGNED_BYTE, vs, (const void*)((const nk_byte*)vertices + vc));}

	nk_draw_command* cmd = nullptr;
	offset = (const nk_draw_index*)nk_buffer_memory_const(ebuf);
	nk_draw_foreach(cmd, ctx, cmdbuf)
	{
		if (!cmd->elem_count) continue;
		glBindTexture(GL_TEXTURE_2D, (GLuint)cmd->texture.id);
		glScissor(
			(GLint)(cmd->clip_rect.x * pPvt->scalex),
			(GLint)((pPvt->height - (GLint)(cmd->clip_rect.y + cmd->clip_rect.h)) * pPvt->scaley),
			(GLint)(cmd->clip_rect.w * pPvt->scalex),
			(GLint)(cmd->clip_rect.h * pPvt->scaley));
		glDrawElements(GL_TRIANGLES, (GLsizei)cmd->elem_count, GL_UNSIGNED_SHORT, offset);
		offset += cmd->elem_count;
	}
	nk_clear(ctx);
	nk_buffer_clear(cmdbuf);
	nk_buffer_clear(vbuf);
	nk_buffer_clear(ebuf);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);

	return true;
}

/**
 * Uploads a new texture to the device for rendering
 * @param data
 * @param w
 * @param h
 * @param fmt
 */
uint64_t UI_Nuklear_UploadTexture(const void* data, int w, int h, EImageFormat fmt)
{
	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	return tex;
}
