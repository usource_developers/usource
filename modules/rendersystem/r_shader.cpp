/*
r_shader.cpp - Shader utils
Copyright (C) 2021 Jeremy L

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/

#include "r_local.h"
#include "engine/common/library.h"
#include "beamdef.h"
#include "particledef.h"
#include "entity_types.h"
#include "tier1/vfs.h"

#include <vector>

std::vector<IShader*>& ShaderList()
{
	static std::vector<IShader*> gShaderList;
	return gShaderList;
}

void GL_LoadShaderSource(GLint shader, const char* sourceCode)
{
	std::vector<char*> line;
	char* code = strdup(sourceCode);
	for(char* s = strtok(code, "\n"); s; s = strtok(NULL, "\n"))
	{
		auto len = strlen(s);
		auto l = (char*)malloc(len + 2);
		memcpy(l, s, len);
		l[len] = '\n';
		l[len+1] = 0;
		line.push_back(l);
	}
	glShaderSource(shader, line.size(), line.data(), NULL);

	for(auto x : line)
		free(x);
}

bool GL_LoadShaderSourceFromFile(GLint shader, const char* sourceCode)
{
	size_t sz = 0;
	auto buf = fs::ReadFileToString(sourceCode, sz);

	if(!buf)
		return false;

	if(sz == 0)
	{
		if(buf)
			Q_free(buf);
		return false;
	}

	GL_LoadShaderSource(shader, buf);
	Q_free(buf);
	return true;
}

bool GL_CompileShader(GLint shader, const char* name)
{
	glCompileShader(shader);

	GLint ok = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &ok);
	if(ok != GL_TRUE)
	{
		static GLchar msg[4096];
		GLsizei len = 0;
		glGetShaderInfoLog(shader, sizeof(msg), &len, msg);
		Log::Msg(gRendersystemLogChan, {255, 0, 0}, "Shader %s failed to compile:\n\n%s\n***********************\n", name,
			msg);
		return false;
	}
	return true;
}


bool GL_LinkProgram(GLint program, const char* name)
{
	glLinkProgram(program);

	GLint ok = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &ok);
	if(ok != GL_TRUE)
	{
		static GLchar msg[4096];
		GLsizei len = 0;
		glGetProgramInfoLog(program, sizeof(msg), &len, msg);
		Log::Msg(gRendersystemLogChan, {255, 0, 0}, "Program %s failed to link:\n\n%s\n************************\n", name,
			msg);
		return false;
	}
	return true;
}

void GL_DrawScreenSpaceQuad(GLint program)
{
	glUseProgram(program);

	glBegin(GL_QUADS);
		glVertex2f(-1.f, -1.f);
		glVertex2f(-1.f, 1.f);
		glVertex2f(1.f, 1.f);
		glVertex2f(1.f, -1.f);
	glEnd();

	glUseProgram(0);
}

void GL_RegisterShader(IShader* shader)
{
	ShaderList().push_back(shader);
}

void GL_ReloadShaders()
{
	Log::Msg(gRendersystemLogChan, "Reloading all shaders.\n");
	for(auto shader : ShaderList())
	{
		shader->Reload();
	}
}

void GL_InitShaders()
{
	Log::Msg(gRendersystemLogChan, "Initializing all shaders.\n");
	for(auto shader : ShaderList())
	{
		shader->Init(false);
	}
}

IShader* GL_GetShaderByName(const char* name)
{
	for(auto shader : ShaderList())
	{
		if(!strcmp(shader->GetName(), name))
			return shader;
	}
	return nullptr;
}

/*
=======================================================================

 SHADER HELPERS

=======================================================================
*/

IShader::IShader(const char* name)
{
	m_shaderName = name;
}

void IShader::LoadFragment(const char* filePath)
{
	m_fragPath = filePath;
	if(m_fragShader)
	{
		glDetachShader(m_program, m_fragShader);
		glDeleteShader(m_fragShader);
	}
	m_fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	if(!GL_LoadShaderSourceFromFile(m_fragShader, filePath))
	{
		Log::Msg(gRendersystemLogChan, {255, 0, 0}, "Could not load shader source file '%s'!\n", filePath);
		m_canRender = false;
		return;
	}

	if(!GL_CompileShader(m_fragShader, m_shaderName.c_str()))
	{
		m_canRender = false;
		return;
	}
}

void IShader::LoadVertex(const char* filePath)
{
	m_vertPath = filePath;
	if(m_vertexShader)
	{
		glDetachShader(m_program, m_vertexShader);
		glDeleteShader(m_vertexShader);
	}
	m_vertexShader = glCreateShader(GL_FRAGMENT_SHADER);

	if(!GL_LoadShaderSourceFromFile(m_vertexShader, filePath))
	{
		Log::Msg(gRendersystemLogChan, {255, 0, 0}, "Could not load shader source file '%s'!\n", filePath);
		m_canRender = false;
		return;
	}

	if(!GL_CompileShader(m_vertexShader, m_shaderName.c_str()))
	{
		m_canRender = false;
		return;
	}
}

void IShader::LoadGeometry(const char* filePath)
{
	m_geomPath = filePath;
	if(m_geomShader)
	{
		glDetachShader(m_program, m_geomShader);
		glDeleteShader(m_geomShader);
	}
	m_geomShader = glCreateShader(GL_GEOMETRY_SHADER);

	if(!GL_LoadShaderSourceFromFile(m_geomShader, filePath))
	{
		Log::Msg(gRendersystemLogChan, {255, 0, 0}, "Could not load shader source file '%s'!\n", filePath);
		m_canRender = false;
		return;
	}

	if(!GL_CompileShader(m_geomShader, m_shaderName.c_str()))
	{
		m_canRender = false;
		return;
	}
}

bool IShader::CanRender()
{
	return m_canRender;
}

void IShader::Reload()
{
	Cleanup(); // Cleanup all remaining GL state
	InternalInit(true); // And just initialize everything again
}

void IShader::Cleanup()
{
	if(m_program)
	{
		if(m_fragShader)
		{
			glDetachShader(m_program, m_fragShader);
		}
		if(m_geomShader)
		{
			glDetachShader(m_program, m_geomShader);
		}
		if(m_vertexShader)
		{
			glDetachShader(m_program, m_vertexShader);
		}
		glDeleteProgram(m_program);
		m_program = 0;
	}
	if(m_fragShader)
	{
		glDeleteShader(m_fragShader);
		m_fragShader = 0;
	}
	if(m_geomShader)
	{
		glDeleteShader(m_geomShader);
		m_geomShader = 0;
	}
	if(m_vertexShader)
	{
		glDeleteShader(m_vertexShader);
		m_vertexShader = 0;
	}
}


void IShader::InternalInit(bool reload)
{
	m_program = glCreateProgram();

	this->Init(reload);

	if(this->m_canRender)
	{
		if(m_fragShader)
		{
			glAttachShader(m_program, m_fragShader);
		}
		if(m_vertexShader)
		{
			glAttachShader(m_program, m_vertexShader);
		}
		if(m_geomShader)
		{
			glAttachShader(m_program, m_geomShader);
		}

		// Try to link now
		if(!GL_LinkProgram(m_program, m_shaderName.c_str()))
		{
			m_canRender = false;
			Log::Msg(gRendersystemLogChan, {255, 0, 0}, "Could not load shader program '%s' due to errors\n",
				m_shaderName.c_str());
		}
	}

	this->SetupAttribs();

}

void IShader::BeginRender()
{
	glUseProgram(m_program);
}

void IShader::EndRender()
{
	glUseProgram(0);
}


GLint IShader::GetUniformLocation(const char* uniform)
{
	return glGetUniformLocation(m_program, uniform);
}

GLint IShader::GetAttribLocation(const char* attrib)
{
	return glGetAttribLocation(m_program, attrib);
}


/*
=======================================================================

 MESH HELPERS

=======================================================================
*/

CMesh::CMesh()
{
	glGenBuffers(1, &m_indexBuffer);
	glGenBuffers(1, &m_vertexBuffer);
}

CMesh::~CMesh()
{
	glDeleteBuffers(1, &m_indexBuffer);
	glDeleteBuffers(1, &m_vertexBuffer);
}

void CMesh::SetVBOData(const void* data, size_t length)
{
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, length, data, GL_DYNAMIC_DRAW);
	m_vboSize = length;
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void CMesh::SetIBOData(const void* data, size_t length)
{
	glBindBuffer(GL_ARRAY_BUFFER, m_indexBuffer);
	glBufferData(GL_ARRAY_BUFFER, length, data, GL_DYNAMIC_DRAW);
	m_iboSize = length;
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void CMesh::InitVBOStorage(size_t length)
{
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, length, nullptr, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void CMesh::InitIBOStorage(size_t length)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, length, nullptr, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}


void* CMesh::LockVBO(size_t offset)
{
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	m_vbolocked = true;
	void* data = glMapBufferRange(GL_ARRAY_BUFFER, offset, m_vboSize - offset, GL_MAP_WRITE_BIT | GL_MAP_READ_BIT);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return data;
}

void* CMesh::LockVBOPartial(size_t offset, size_t length)
{
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	m_vbolocked = true;
	void* data = glMapBufferRange(GL_ARRAY_BUFFER, offset, length, GL_MAP_WRITE_BIT | GL_MAP_READ_BIT);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return data;
}

void CMesh::UnlockVBO()
{
	m_vbolocked = false;
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void* CMesh::LockIBO(size_t offset)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
	m_ibolocked = true;
	void* data = glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, offset, m_iboSize - offset, GL_MAP_WRITE_BIT | GL_MAP_READ_BIT);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	return data;
}

void* CMesh::LockIBOPartial(size_t offset, size_t length)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
	m_ibolocked = true;
	void* data = glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, offset, length, GL_MAP_WRITE_BIT | GL_MAP_READ_BIT);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	return data;
}

void CMesh::UnlockIBO()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
	m_ibolocked = false;
	glUnmapBuffer(m_indexBuffer);
}

void CMesh::CopyToVBO(const void* data, size_t offset, size_t length)
{
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glBufferSubData(GL_ARRAY_BUFFER, offset, length, data);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void CMesh::CopyToIBO(const void* data, size_t offset, size_t length)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, length, data);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}


//==================================================================================================================================//

CMeshBuilder::CMeshBuilder(CMesh* mesh, size_t bytes = 8192)
{
	m_data = (vertex_format_t*)malloc(bytes);
	m_writeOffset = 0;
	m_writeHead = 0;
	m_mesh = mesh;
	m_currentElem = &m_data[0];
	m_elemCount = 1;
}

CMeshBuilder::~CMeshBuilder()
{
	free(m_data);
}

void CMeshBuilder::WriteChanges()
{
	m_mesh->CopyToVBO(m_data, m_writeOffset, m_length);
}

void CMeshBuilder::Begin(EMeshBuilderMode mode)
{

}

void CMeshBuilder::End()
{

}

/* Specify vertex normals */
void CMeshBuilder::Normal3fv(const float* normal)
{
	VectorCopy(normal, m_currentElem->normal);
}

void CMeshBuilder::Normal3f(float x, float y, float z)
{
	VectorSet(m_currentElem->normal, x, y, z);
}


/* Specify actual verticies */
void CMeshBuilder::Vertex2v(const float* vertex)
{
	Vector2Copy(vertex, m_currentElem->vertex);
}

void CMeshBuilder::Vertex2f(float x, float y)
{
	Vector2Set(m_currentElem->vertex, x, y);
}

void CMeshBuilder::Vertex3v(const float* vertex)
{
	VectorCopy(vertex, m_currentElem->vertex);
}

void CMeshBuilder::Vertex3f(float x, float y, float z)
{
	VectorSet(m_currentElem->vertex, x, y, z);
}

void CMeshBuilder::Vertex4v(const float* vertex)
{
	Vector4Copy(vertex, m_currentElem->vertex);
}

void CMeshBuilder::Vertex4f(float x, float y, float z, float w)
{
	Vector4Set(m_currentElem->vertex, x, y, z, w);
}

/* Specify texture coords */
void CMeshBuilder::TexCoord1f(float f)
{
	m_currentElem->texcoord[0] = f;
}

void CMeshBuilder::TexCoord2f(float x, float y)
{
	Vector2Set(m_currentElem->texcoord, x, y);
}

void CMeshBuilder::TexCoord2fv(const float* coord)
{
	Vector2Copy(coord, m_currentElem->texcoord);
}

void CMeshBuilder::TexCoord3f(float x, float y, float z)
{
	VectorSet(m_currentElem->texcoord, x,  y, z);
}

void CMeshBuilder::TexCoord3fv(const float* coord)
{
	VectorCopy(coord, m_currentElem->texcoord);
}

void CMeshBuilder::TexCoord4f(float x, float y, float z, float w)
{
	Vector4Set(m_currentElem->texcoord, x, y, z, w);
}

void CMeshBuilder::TexCoord4fv(const float* coord)
{
	Vector4Copy(coord, m_currentElem->texcoord);
}


void CMeshBuilder::Advance()
{
	m_elemCount++;
	m_writeHead++;
	m_currentElem = &m_data[m_writeHead];
	memset(m_currentElem, 0, sizeof(vertex_format_t));
}
