// Main header for UI rendering
#pragma once


#include "r_local.h"

bool UI_Nuklear_Draw(void* privateData);
uint64_t UI_Nuklear_UploadTexture(const void* data, int w, int h, EImageFormat fmt);
