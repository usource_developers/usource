#ifndef _ENGINE_PCH_H_
#define _ENGINE_PCH_H_

#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <vector>
#include <math.h>

#ifdef _POSIX
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#endif

#include "public/containers/array.h"
#include "public/containers/list.h"
#include "public/crtlib.h"
#include "public/threadtools.h"
#include "public/keyvalues.h"
#include "public/xprof.h"
#include "public/debug.h"
#include "public/appframework.h"
#include "public/logger.h"

#include "tier1/convar.h"
#include "tier1/concommand.h"
#include "tier1/dbg.h"

#include "mathlib/mathlib.h"
#include "mathlib/vector.h"
#include "mathlib/matrix.h"

#endif //_ENGINE_PCH_H_