/**

 Backend renderer for Nuklear UI

 */
#include "NkBackend.h"
#include "EngineCallback.h"

/* Crt includes */
#include "crtlib.h"
#include "keydefs.h"


#define NK_IMPLEMENTATION
#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#include "nuklear.h"

// Allocate 16MB for now
#define NUKLEAR_MEMORY (16384 * 1024)

/* Dirty dirty globals... */
nk_context g_NuklearContext;

nk_buffer gNkVBuf;
nk_buffer gNkEBuf;
nk_buffer gNkCmdBuf;
nk_font_atlas gNkFontAtlas;

nk_draw_null_texture gNkNullTex;

void ui::NkInit()
{
	nk_user_font fnt;

	nk_init_fixed(&g_NuklearContext, Q_malloc(NUKLEAR_MEMORY), NUKLEAR_MEMORY, &fnt);

	nk_buffer_init_default(&gNkEBuf);
	nk_buffer_init_default(&gNkVBuf);
	nk_buffer_init_default(&gNkCmdBuf);

	nk_font_atlas_init_default(&gNkFontAtlas);

	int w, h;
	const void* img = nk_font_atlas_bake(&gNkFontAtlas, &w, &h, NK_FONT_ATLAS_RGBA32);
	auto tex = GlobalRenderUIBackend().UploadTexture(RENDERUI_BACKEND_NUKLEAR, img, w, h, EImageFormat::RGBA8);
	nk_font_atlas_end(&gNkFontAtlas, nk_handle_id(tex), &gNkNullTex);
	nk_style_set_font(&g_NuklearContext, &gNkFontAtlas.default_font->handle);

	nk_input_begin(&g_NuklearContext);

}

// MUST be called from main thread
void ui::NkFrame()
{
	nk_input_end(&g_NuklearContext);

	nk_begin(&g_NuklearContext, "Fucking shit", nk_rect(0, 0, 500, 500), NK_WINDOW_BORDER | NK_WINDOW_CLOSABLE | NK_WINDOW_MOVABLE);
		nk_layout_row_dynamic(&g_NuklearContext, 50, 1);
	nk_button_text(&g_NuklearContext, "Hello world\n", sizeof("Hello world"));
	nk_button_text(&g_NuklearContext, "Hello world\n", sizeof("Hello world"));
	nk_button_text(&g_NuklearContext, "Hello world\n", sizeof("Hello world"));
	nk_button_text(&g_NuklearContext, "Hello world\n", sizeof("Hello world"));
	nk_button_text(&g_NuklearContext, "Hello world\n", sizeof("Hello world"));
	nk_button_text(&g_NuklearContext, "Hello world\n", sizeof("Hello world"));
	nk_button_text(&g_NuklearContext, "Hello world\n", sizeof("Hello world"));
	nk_button_text(&g_NuklearContext, "Hello world\n", sizeof("Hello world"));
	nk_end(&g_NuklearContext);

	RenderUINuklearPvt_t ctx;
	ctx.ctx = &g_NuklearContext;
	ctx.ebuf = &gNkEBuf;
	ctx.vbuf = &gNkVBuf;
	ctx.scalex = ctx.scalex = 1.0f;
	ctx.cmdbuf = &gNkCmdBuf;

	ui::GlobalRenderUIBackend().Draw(RENDERUI_BACKEND_NUKLEAR, &ctx);

	nk_input_begin(&g_NuklearContext);
}

void ui::NkShutdown()
{

}

void ui::NkPrecache()
{

}

void ui::NkVidInit()
{

}


void ui::NkMouseMoveEvent(int x, int y)
{
	nk_input_motion(&g_NuklearContext, x, y);
}

void ui::NkKeyEvent(int key, bool down)
{
	if(key == K_MOUSE1)
	{
		int x, y;
		ui::GetCursorPos(x,y);
		nk_input_button(&g_NuklearContext, nk_buttons::NK_BUTTON_LEFT, x, y, down);
	}
}

void ui::NkCharEvent(int key)
{
	nk_input_char(&g_NuklearContext, (char)key);
}
