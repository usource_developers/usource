
#pragma once

#include "MainUI.h"

BEGIN_MAINUI_NAMESPACE

void NkInit();
void NkFrame();
void NkShutdown();
void NkVidInit();
void NkPrecache();

void NkMouseMoveEvent(int x, int y);
void NkKeyEvent(int key, bool down);
void NkCharEvent(int key);

END_MAINUI_NAMESPACE