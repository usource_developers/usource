// Test code for libpublic

#include "crtlib.h"
#include "crclib.h"
#include "public.h"
#include "threadtools.h"
#include "keyvalues.h"
#include "logger.h"

#include "public_test.h"

CUnitTestSuite* gpPublicTestSuite = nullptr;

int main(int argc, char** argv)
{
	auto chan = Log::CreateChannel("PublicTest", {0, 255, 0});
	Log::Log(chan, ELogLevel::MSG_GENERAL, "libpublic test suite\n");

	gpPublicTestSuite = CUnitTestSuite::Create("General LibPublic Tests", chan);

	auto test = gpPublicTestSuite->CreateTest("Test!");
	test->MustBeEqual(1.4f, 1.0f, 0.4f, "aaa");

	gpPublicTestSuite->Submit(test);

	gpPublicTestSuite->Report();

	gpPublicTestSuite->Destroy();
}