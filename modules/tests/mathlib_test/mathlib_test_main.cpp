// Test code for libpublic

#include "crtlib.h"
#include "crclib.h"
#include "public.h"
#include "threadtools.h"
#include "keyvalues.h"
#include "logger.h"

#include "mathlib/mathlib.h"

#include "unittestlib.h"

CUnitTestSuite* gpMathlibTestSuite = nullptr;

int main(int argc, char** argv)
{
	auto chan = Log::CreateChannel("PublicTest", {0, 255, 0});
	Log::Log(chan, ELogLevel::MSG_GENERAL, "libpublic test suite\n");

	gpMathlibTestSuite = CUnitTestSuite::Create("General Mathlib Tests", chan);

	/* Testing of rsqrt */
	auto rsqrtTest = gpMathlibTestSuite->CreateTimedTest("RSQRT");
	{
		/* Need to generate random test data */
		const int ITERATIONS = 1000000;
		float data[ITERATIONS];
		for(int i = 0; i < ITERATIONS; i++) {
			data[i] = (float)rand() / (float)rand();
		}

		rsqrtTest->IteratedTest<float>(
		[]() -> float {
			return (float)rand() / (float)rand();
		},
		[](float x) -> void {
			rsqrt(x);
		}, ITERATIONS, "rsqrt");
	}
	gpMathlibTestSuite->Submit(rsqrtTest);


	/* Testing of old rsqrt */
	auto rsqrtcTest = gpMathlibTestSuite->CreateTimedTest("RSQRT_C");
	{
		/* Need to generate random test data */
		const int ITERATIONS = 1000000;
		float data[ITERATIONS];
		for(int i = 0; i < ITERATIONS; i++) {
			data[i] = (float)rand() / (float)rand();
		}

		rsqrtcTest->IteratedTest<float>(
		[]() -> float {
			return (float)rand() / (float)rand();
		},
		[](float x) -> void {
			rsqrt_c(x);
		}, ITERATIONS, "rsqrt_c");
	}
	gpMathlibTestSuite->Submit(rsqrtcTest);

	gpMathlibTestSuite->Report();

	gpMathlibTestSuite->Destroy();
}
