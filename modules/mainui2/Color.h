#pragma once

#include "MainUI2.h"

BEGIN_MAINUI_NAMESPACE

struct Color
{
	unsigned char r, g, b, a;

	Color(unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a = 255) :
		r(_r), g(_g), b(_b), a(_a)
	{
	}

	Color()
	{
		r = g = b = a = 0;
	}



	unsigned int Pack() const
	{
		unsigned int res = 0;
		res |= (r << 24 & 0xFF000000);
		res |= (g << 16 & 0x00FF0000);
		res |= (b << 8 & 0x0000FF00);
		res |= (a & 0x000000FF);
		return res;
	}

	void Unpack(unsigned int color)
	{
		a = color & 0x000000FF;
		b = (color & 0x0000FF00) >> 8;
		g = (color & 0x00FF0000) >> 16;
		r = (color & 0xFF000000) >> 24;
	}

	static Color White()
	{
		return Color(255, 255, 255, 255);
	}

	static Color Black()
	{
		return Color(0,0,0,255);
	}
};


END_MAINUI_NAMESPACE
