
#pragma once

#include "MainUI2.h"

BEGIN_MAINUI_NAMESPACE

struct Point
{
	int x, y;
};

struct Size
{
	int w, h;

	Size()
	{
		w = h = 0;
	}

	Size(int _w, int _h)
	{
		w = _w;
		h = _h;
	}

	Size(float _w, float _h)
	{
		w = (int)_w;
		h = (int)_h;
	}
};

END_MAINUI_NAMESPACE