
#include "common.h"
#include "ref_int.h"
#include "menu_int.h"

#include "tier1/tier1.h"
#include "tier1/dbg.h"
#include "tier1/concommand.h"
#include "public/logger.h"
#include "netadr.h"

#include "MainUI2.h"
#include "NkBackend.h"

ui_enginefuncs_t   gEngineFuncs;
ui_extendedfuncs_t gEngineExtFuncs;
ui_globalvars_t *gpGlobals;
bool gMenuVisible;

struct UIGlobals_t
{
	int cursorX, cursorY;
	bool cursorVisible;
	bool menuVisible = false;
	bool menuActive = false;
};

UIGlobals_t gUIGlobals;

static UI_FUNCTIONS gFunctionTable = {
	ui::VidInit, ui::Init, ui::Shutdown, ui::Redraw, ui::KeyEvent, ui::MouseMove, ui::SetActiveMenu,
	ui::AddServerToList,
	ui::GetCursorPos, ui::SetCursorPos, ui::ShowCursor, ui::CharEvent, ui::MouseInRect, ui::IsVisible,
	ui::CreditsActive, ui::FinalCredits};

int ui::VidInit()
{
	NkVidInit();

	return 1;
}

void ui::Init()
{
	tier1::Connect();

	ui::NkInit();
}

void ui::Shutdown()
{
	ui::NkShutdown();
}

void ui::Redraw(float flTime)
{
	ui::NkFrame();
}

void ui::KeyEvent(int key, int down)
{
	ui::NkKeyEvent(key, down);
}

void ui::MouseMove(int x, int y)
{
	gUIGlobals.cursorX = x;
	gUIGlobals.cursorY = y;

	ui::NkMouseMoveEvent(x, y);
}

void ui::SetActiveMenu(int active)
{
	if(active)
		gUIGlobals.menuActive = gUIGlobals.menuVisible = true;
	else
		gUIGlobals.menuActive = gUIGlobals.menuVisible = false;
}

void ui::AddServerToList(struct netadr_s adr, const char *info)
{

}

void ui::GetCursorPos(int *pos_x, int *pos_y)
{
	if(*pos_x)
		*pos_x = gUIGlobals.cursorX;
	if(*pos_y)
		*pos_y = gUIGlobals.cursorY;
}

void ui::SetCursorPos(int pos_x, int pos_y)
{
	gUIGlobals.cursorY = pos_y;
	gUIGlobals.cursorX = pos_x;
}

void ui::ShowCursor(int show)
{
	// STUB
}

void ui::CharEvent(int key)
{
	ui::NkCharEvent(key);
}

int ui::MouseInRect()
{
	return ui::CursorInRect(0, 0, ScreenWidth(), ScreenHeight());
}

int ui::IsVisible()
{
	return gUIGlobals.menuVisible;
}

int ui::CreditsActive()
{
	return 0;
}

void ui::FinalCredits()
{

}

ui_enginefuncs_t& ui::EngineFuncs()
{
	return gEngineFuncs;
}

ui_extendedfuncs_s& ui::EngineExtFuncs()
{
	return gEngineExtFuncs;
}

IRenderBackend &ui::GlobalRenderBackend()
{
	static IRenderBackend* gpRenderBackend = nullptr;
	if(!gpRenderBackend)
	{
		gpRenderBackend = static_cast<IRenderBackend*>(AppFramework::FindInterface(IREFINT_INTERFACE));

		if(!gpRenderBackend)
		{
			FatalError("Failed to load the interface %s!!!!!! Mainui2 cannot initialize!\n", IREFINT_INTERFACE);
		}
	}

	return *gpRenderBackend;
}

IRenderUIBackend &ui::GlobalRenderUIBackend()
{
	static IRenderUIBackend* gpRenderUIBackend = nullptr;
	if(!gpRenderUIBackend)
	{
		gpRenderUIBackend = static_cast<IRenderUIBackend*>(AppFramework::FindInterface(IRENDER_UI_INTERFACE));

		if(!gpRenderUIBackend)
		{
			FatalError("Failed to load the interface %s! Cannot initialize mainui!\n", IRENDER_UI_INTERFACE);
		}
	}
	return *gpRenderUIBackend;
}

bool ui::IsUIVisible()
{
	return gUIGlobals.menuVisible;
}

bool ui::CursorInRect(int x1, int y1, int x2, int y2)
{
	return (gUIGlobals.cursorY <= y1 && gUIGlobals.cursorY >= y2 &&
		gUIGlobals.cursorX <= x2 && gUIGlobals.cursorX >= x1);
}

int ui::ScreenWidth()
{
	return gpGlobals->scrWidth;
}

int ui::ScreenHeight()
{
	return gpGlobals->scrHeight;
}

//=======================================================================
//			GetApi
//=======================================================================
extern "C" EXPORT int
GetMenuAPI(UI_FUNCTIONS *pFunctionTable, ui_enginefuncs_t *pEngfuncsFromEngine, ui_globalvars_t *pGlobals)
{
	if (!pFunctionTable || !pEngfuncsFromEngine)
	{
		return false;
	}

	// copy HUD_FUNCTIONS table to engine, copy engfuncs table from engine
	memcpy(pFunctionTable, &gFunctionTable, sizeof(UI_FUNCTIONS));
	memcpy(&gEngineFuncs, pEngfuncsFromEngine, sizeof(ui_enginefuncs_t));
	memset(&gEngineExtFuncs, 0, sizeof(ui_extendedfuncs_t));

	gpGlobals = pGlobals;

	return true;
}

class CMainUI001 : public IMainUIInterface
{
public:
	const char *GetParentInterface() override
	{
		return IMAINUI_INTERFACE;
	};

	const char *GetName() override
	{
		return "CMainUI001";
	};

	bool PreInit() override
	{
		return true;
	};

	bool Init() override
	{
		return true;
	};

	void Shutdown() override
	{
	};

	int VidInit(void) override;

	void UIInit(void) override;

	void UIShutdown(void) override;

	void Redraw(float flTime) override;

	void KeyEvent(int key, int down) override;

	void MouseMove(int x, int y) override;

	void SetActiveMenu(int active) override;

	void AddServerToList(struct netadr_s adr, const char *info) override;

	void GetCursorPos(int *pos_x, int *pos_y) override;

	void SetCursorPos(int pos_x, int pos_y) override;

	void ShowCursor(int show) override;

	void CharEvent(int key) override;

	int MouseInRect(void) override;

	int IsVisible(void) override;

	int CreditsActive(void) override;

	void FinalCredits(void) override;

	void ResetPing(void) override;

	void ShowConnectionWarning(void) override;

	void ShowUpdateDialog(int preferStore) override;

	void ShowMessageBox(const char *text) override;

	void ConnectionProgress_Disconnect(void) override;

	void ConnectionProgress_Download(const char *pszFileName, const char *pszServerName, int iCurrent, int iTotal,
	                                 const char *comment) override;

	void ConnectionProgress_DownloadEnd(void) override;

	void ConnectionProgress_Precache(void) override;

	void ConnectionProgress_Connect(const char *server) override;

	void ConnectionProgress_ChangeLevel(void) override;

	void ConnectionProgress_ParseServerInfo(const char *server) override;
};

EXPOSE_INTERFACE(CMainUI001);
MODULE_INTERFACE_IMPL();

int CMainUI001::VidInit(void)
{
	return ui::VidInit();
}

void CMainUI001::UIInit(void)
{
	ui::Init();
}

void CMainUI001::UIShutdown(void)
{
	ui::Shutdown();
}

void CMainUI001::Redraw(float flTime)
{
	ui::Redraw(flTime);
}

void CMainUI001::KeyEvent(int key, int down)
{
	ui::KeyEvent(key, down);
}

void CMainUI001::MouseMove(int x, int y)
{
	ui::MouseMove(x, y);
}

void CMainUI001::SetActiveMenu(int active)
{
	ui::SetActiveMenu(active);
}

void CMainUI001::AddServerToList(struct netadr_s adr, const char *info)
{
	ui::AddServerToList(adr, info);
}

void CMainUI001::GetCursorPos(int *pos_x, int *pos_y)
{
	ui::GetCursorPos(pos_x, pos_y);
}

void CMainUI001::SetCursorPos(int pos_x, int pos_y)
{
	ui::SetCursorPos(pos_x, pos_y);
}

void CMainUI001::ShowCursor(int show)
{
	ui::ShowCursor(show);
}

void CMainUI001::CharEvent(int key)
{
	ui::CharEvent(key);
}

int CMainUI001::MouseInRect(void)
{
	return ui::MouseInRect();
}

int CMainUI001::IsVisible(void)
{
	return ui::IsVisible();
}

int CMainUI001::CreditsActive(void)
{
	return ui::CreditsActive();
}

void CMainUI001::FinalCredits(void)
{
	ui::FinalCredits();
}

void CMainUI001::ResetPing(void)
{
}

void CMainUI001::ShowConnectionWarning(void)
{
}

void CMainUI001::ShowUpdateDialog(int preferStore)
{
}

void CMainUI001::ShowMessageBox(const char *text)
{
}

void CMainUI001::ConnectionProgress_Disconnect(void)
{
}

void
CMainUI001::ConnectionProgress_Download(const char *pszFileName, const char *pszServerName, int iCurrent, int iTotal,
                                        const char *comment)
{
}

void CMainUI001::ConnectionProgress_DownloadEnd(void)
{
}

void CMainUI001::ConnectionProgress_Precache(void)
{
}

void CMainUI001::ConnectionProgress_Connect(const char *server)
{
}

void CMainUI001::ConnectionProgress_ChangeLevel(void)
{
}

void CMainUI001::ConnectionProgress_ParseServerInfo(const char *server)
{
}
