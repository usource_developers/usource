#include "MainUI2.h"

#include "Panel.h"
#include "Button.h"
#include "layouts/WidgetGroup.h"
#include "layouts/GridLayout.h"

#include "tier1/concommand.h"
#include "tier1/convar.h"

using namespace ui;

class CMainMenu : public ui::CPanel
{
public:

	CMainMenu() : CPanel("MainMenuPanel")
	{
		/* Set properties of this window */
		this->SetFullscreen(true);
		this->SetActive(true);

		/* Add a grid layout */
		CGridLayout* gridLayout = new CGridLayout(this);
		gridLayout->SetColumns(2);

		/* Add a button to the window */
		CButton* button = new CButton(gridLayout, "Hello!!");
		button->SetTextColor(Color(255, 255, 255, 255));
		button->SetHoverColor(Color(0, 255, 0, 255));
		button->SetBorderRadius(1.0f);
		button->SetCallback([](CButton*) {
			      printf("FUck!!\n");
		      });
		gridLayout->AddWidget(*button);
		for(int i = 0; i < 10; i++)
			gridLayout->AddWidget(*button);

		this->AddLayout(*gridLayout);
	}
};

CONCOMMAND(menu_main, "Show main menu", 0)
{
	static CMainMenu mainMenu;
}
