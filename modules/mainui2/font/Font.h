#pragma once

#include "MainUI2.h"

#include "containers/list.h"

/* Freetype/FontConfig headers */
#include "fontconfig/fontconfig.h"
#include "fontconfig/fcfreetype.h"
#include "freetype2/ft2build.h"

BEGIN_MAINUI_NAMESPACE


class CFont
{
private:

	// Font atlas image data. ALWAYS RGBA32 format (8-bit per channel)
	void* m_imageData;
public:
};


class CFontManager
{
private:
	List<CFont*> m_fonts;
public:

	CFont* CreateFont(const char* identifier, const char* font_name, int size);

	CFont* FindFont(const char* identifier);

};

END_MAINUI_NAMESPACE