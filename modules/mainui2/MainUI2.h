#ifndef _MAINUI_2_H_
#define _MAINUI_2_H_

#include "common.h"
#include "const.h"
#include "cvardef.h"
#include "ref_int.h"
#include "gui_int.h"
#include "menu_int.h"


#define BEGIN_MAINUI_NAMESPACE namespace ui {
#define END_MAINUI_NAMESPACE }

BEGIN_MAINUI_NAMESPACE

int VidInit();
void Init();
void Shutdown();
void Redraw(float flTime);
void KeyEvent(int key, int down);
void MouseMove(int x, int y);
void SetActiveMenu(int active);
void AddServerToList(struct netadr_s adr, const char* info);
void GetCursorPos(int* pos_x, int* pos_y);
void SetCursorPos(int pos_x, int pos_y);
void ShowCursor(int show);
void CharEvent(int key);
int MouseInRect(); // mouse entering\leave game window
int IsVisible();
int CreditsActive(); // unused
void FinalCredits(); // show credits + game end

ui_enginefuncs_t& EngineFuncs();
ui_extendedfuncs_s& EngineExtFuncs();

IRenderBackend &GlobalRenderBackend();
IRenderUIBackend &GlobalRenderUIBackend();

bool IsUIVisible();
int ScreenWidth();
int ScreenHeight();
bool CursorInRect(int x1, int y1, int x2, int y2);

enum class ETextAlignment
{
	LEFT = 0,
	RIGHT,
	CENTER
};

END_MAINUI_NAMESPACE

#endif