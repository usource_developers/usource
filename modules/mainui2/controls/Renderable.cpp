#include "Renderable.h"
#include "Panel.h"
#include "Widget.h"
#include "layouts/Layout.h"

using namespace ui;

bool IRenderable::IsPanel() const
{
	return dynamic_cast<const CPanel*>(this) != nullptr;
}

bool IRenderable::IsWidget() const
{
	return dynamic_cast<const CWidget*>(this) != nullptr;
}

bool IRenderable::IsLayout() const
{
	return dynamic_cast<const CLayout*>(this) != nullptr;
}
