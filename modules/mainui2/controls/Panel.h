

#pragma once

#include "MainUI2.h"
#include "Point.h"
#include "Renderable.h"
#include "Scheme.h"

#include "containers/array.h"
#include "containers/string.h"

BEGIN_MAINUI_NAMESPACE

class CWidget;
class CLayout;

/**
 * Base Class for all "Windows"
 */
class CPanel : public IRenderable
{
private:
	List<CLayout*> m_widgets;
	String m_title;
	String m_name;
	bool m_fullscreen = false;
	bool m_movable = false;
	bool m_resizable = false;
	bool m_closable = false;
	bool m_scrollable = false;
	bool m_minimizable = false;
	bool m_bordered = false;
	Point m_pos;
	Size m_size;
	unsigned long long m_flags;
	CScheme* m_scheme;

	void UpdateFlags();

	CPanel();
public:
	explicit CPanel(const char* name, const char* title = "");
	~CPanel();

	const char* Title() const;
	const char* Name() const;
	bool Fullscreen() const;
	int Width() const;
	int Height() const;
	Size GetSize();
	Point GetPos();
	inline bool Movable() { return m_movable; };
	inline bool Resizable() { return m_resizable; };
	inline bool Closable() { return m_closable; };
	inline bool Scrollable() { return m_scrollable; };
	inline bool Minimizable() { return m_minimizable; }
	inline bool Bordered() { return m_bordered; }

	void SetFullscreen(bool fs);
	void SetTitle(const char* title);
	void SetDimensions(int w, int h);
	void SetPos(int x, int y);
	void SetMovable(bool movable);
	void SetResizable(bool resizable);
	void SetClosable(bool closable);
	void SetScrollable(bool scrollable);
	void SetMinimizable(bool minimizable);
	void SetBordered(bool bordered);

	void AddLayout(CLayout& widget);
	void RemoveLayout(CLayout& widget);

	void SetScheme(CScheme& scheme) { m_scheme = &scheme; };
	CScheme& GetScheme() { return *m_scheme; };

	virtual void Draw(void* drawData);
};

END_MAINUI_NAMESPACE
