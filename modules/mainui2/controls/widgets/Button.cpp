#include "Button.h"
#include "NkBackend.h"
#include "Panel.h"
#include "Scheme.h"
#include "layouts/Layout.h"

struct ButtonNkData_t
{
	nk_style_button btn;
};

ui::CButton::CButton(ui::CLayout *parent) :
	CWidget(parent)
{
	m_nkData = malloc(sizeof(ButtonNkData_t));
	memset(m_nkData, 0, sizeof(ButtonNkData_t));
	this->Init(parent);
}


ui::CButton::CButton(ui::CLayout *parent, const char *text) :
	CWidget(parent),
	m_text(text)
{
	m_nkData = malloc(sizeof(ButtonNkData_t));
	memset(m_nkData, 0, sizeof(ButtonNkData_t));
	this->Init(parent);
}

void ui::CButton::Init(IRenderable* _parent)
{
	CPanel* parent = &((CLayout*)m_parent)->Window();
	auto scheme = parent->GetScheme();
	ButtonNkData_t* pData = (ButtonNkData_t*)m_nkData;

	pData->btn.normal = ColorToStyleItem(scheme.GetElementColor(ESchemeColor::COLOR_BUTTON));
	pData->btn.hover = ColorToStyleItem(scheme.GetElementColor(ESchemeColor::COLOR_BUTTON_HOVER));
	pData->btn.active = ColorToStyleItem(scheme.GetElementColor(ESchemeColor::COLOR_BUTTON_ACTIVE));
	pData->btn.border_color = ColorToNkColor(Color::Black());
	pData->btn.text_background = ColorToNkColor(Color::Black());
	pData->btn.text_normal = ColorToNkColor(scheme.GetElementColor(ESchemeColor::COLOR_TEXT));
	pData->btn.text_active = ColorToNkColor(Color::White());
	pData->btn.text_hover = ColorToNkColor(Color::White());
	pData->btn.text_alignment = NK_TEXT_CENTERED;
	pData->btn.border = 0.1f;
	pData->btn.rounding = 5.0f;
	pData->btn.padding = nk_vec2(.0f, .0f);
	pData->btn.image_padding = nk_vec2(.0f, .0f);
	pData->btn.touch_padding = nk_vec2(.0f, .0f);

}


void ui::CButton::SetBackgroundColor(ui::Color color)
{
	ButtonNkData_t* pData = (ButtonNkData_t*)m_nkData;
	pData->btn.text_background = ColorToNkColor(color);
	m_backgroundColor = color;
}

void ui::CButton::SetBorderRadius(float radius)
{
	ButtonNkData_t* pData = (ButtonNkData_t*)m_nkData;
	pData->btn.rounding = radius;
	m_borderRadius = radius;
}

void ui::CButton::SetHoverColor(ui::Color c)
{
	ButtonNkData_t* pData = (ButtonNkData_t*)m_nkData;
	pData->btn.hover.data.color = ColorToNkColor(c);
	pData->btn.hover.type = NK_STYLE_ITEM_COLOR;
	m_hoverColor = c;
}

void ui::CButton::SetPressColor(ui::Color c)
{
	ButtonNkData_t* pData = (ButtonNkData_t*)m_nkData;
	pData->btn.active.data.color = ColorToNkColor(c);
	pData->btn.active.type = NK_STYLE_ITEM_COLOR;
	m_pressColor = c;
}

void ui::CButton::SetText(const char *text)
{
	m_text = text;
}

void ui::CButton::SetBorderColor(ui::Color c)
{
	ButtonNkData_t* pData = (ButtonNkData_t*)m_nkData;
	pData->btn.border_color = ColorToNkColor(c);
	m_borderColor = c;
}

void ui::CButton::SetBorder(float f)
{
	ButtonNkData_t* pData = (ButtonNkData_t*)m_nkData;
	pData->btn.border = f;
	m_border = f;
}


void ui::CButton::SetTextAlignment(ui::ETextAlignment align)
{
	ButtonNkData_t* pData = (ButtonNkData_t*)m_nkData;
	switch(align)
	{
		case ui::ETextAlignment::CENTER:
			pData->btn.text_alignment = NK_TEXT_CENTERED;
			break;
		case ui::ETextAlignment::LEFT:
			pData->btn.text_alignment = NK_TEXT_LEFT;
			break;
		default:
			pData->btn.text_alignment = NK_TEXT_RIGHT;
			break;
	}
	m_textAlign = align;

}

void ui::CButton::SetTextColor(ui::Color c)
{
	ButtonNkData_t* pData = (ButtonNkData_t*)m_nkData;
	pData->btn.text_normal = ColorToNkColor(c);
	m_textColor = c;
}

void ui::CButton::Draw(void *drawData)
{
	CWidget::Draw(drawData);

	if(!Active())
		return;

	nk_context* pCtx = (nk_context*)drawData;
	ButtonNkData_t* pData = (ButtonNkData_t*)m_nkData;

	bool pressed = false;
	if(!m_text.empty())
	{
		pressed = nk_button_label_styled(pCtx, &pData->btn, m_text.c_str());
	}

	if(pressed)
		m_callback(this);
}

void ui::CButton::SetCallback(ui::CButton::CallbackT callback)
{
	m_callback = std::move(callback);
}

void ui::CButton::ClearCallback()
{
	m_callback = [](CButton*){};
}
