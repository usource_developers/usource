
#pragma once

#include "MainUI2.h"
#include "Renderable.h"
#include "Point.h"

BEGIN_MAINUI_NAMESPACE

class CPanel;
class CLayout;

class CWidget : public IRenderable
{
protected:
	Point m_pos;
	Size m_size;
	friend class CLayout;
	CPanel* m_window;
public:
	CWidget(CLayout* parent);

	virtual ~CWidget() {};

	void Draw(void* pPvt) override;
};

END_MAINUI_NAMESPACE
