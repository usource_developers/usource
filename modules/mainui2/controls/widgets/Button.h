#pragma once

#include "MainUI2.h"
#include "Widget.h"
#include "Color.h"

#include "containers/string.h"

#include <functional>

BEGIN_MAINUI_NAMESPACE

class CLayout;

class CButton : public CWidget
{
private:
	using CallbackT = std::function<void(CButton*)>;

	Color m_backgroundColor;
	Color m_hoverColor;
	Color m_pressColor;
	String m_text;
	float m_border;
	Color m_borderColor;
	ETextAlignment m_textAlign;
	Color m_textColor;
	float m_borderRadius;

	CallbackT m_callback;

	void* m_nkData;

	void Init(IRenderable* parent);

public:
	CButton(CLayout* parent);
	CButton(CLayout* parent, const char* text);

	Color BackgroundColor() const { return m_backgroundColor; };
	Color HoverColor() const { return m_hoverColor; };
	Color PressColor() const { return m_pressColor; };
	Color BorderColor() const { return m_borderColor; };
	float Border() const { return m_border; };
	Color TextColor() const { return m_textColor; };
	ETextAlignment TextAlignment() const { return m_textAlign; };
	float BorderRadius() const { return m_borderRadius; };

	void SetBackgroundColor(Color color);
	void SetHoverColor(Color c);
	void SetPressColor(Color c);
	void SetBorderColor(Color c);
	void SetBorder(float f);
	void SetTextAlignment(ETextAlignment align);
	void SetTextColor(Color c);
	void SetBorderRadius(float radius);

	void SetText(const char* text);
	const char* GetLabel() { return m_text; };

	void SetCallback(CallbackT callback);
	void ClearCallback();

	void Draw(void* drawData) override;
};

END_MAINUI_NAMESPACE
