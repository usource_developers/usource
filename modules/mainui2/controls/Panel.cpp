
#include "MainUI2.h"
#include "Panel.h"
#include "Widget.h"
#include "layouts/Layout.h"

#include "NkBackend.h"

using namespace ui;

List<ui::CPanel*> *gPanels = nullptr;

ui::CPanel::CPanel(const char *name, const char *title) :
	m_name(name),
	m_title(title),
	IRenderable(nullptr)
{
	m_scheme = CScheme::Empty();

	if(!gPanels)
		gPanels = new List<CPanel*>();
	gPanels->push_back(this);
}

ui::CPanel::~CPanel()
{
	gPanels->remove(this);
}

const char *ui::CPanel::Title() const
{
	return m_title.c_str();
}

const char *ui::CPanel::Name() const
{
	return m_name.c_str();
}

bool ui::CPanel::Fullscreen() const
{
	return m_fullscreen;
}

int ui::CPanel::Width() const
{
	return m_size.w;
}

int ui::CPanel::Height() const
{
	return m_size.h;
}

Size ui::CPanel::GetSize()
{
	return m_size;
}

Point ui::CPanel::GetPos()
{
	return m_pos;
}

void ui::CPanel::SetFullscreen(bool fs)
{
	m_fullscreen = fs;
}

void ui::CPanel::SetTitle(const char *title)
{
	m_title = title;
}

void ui::CPanel::SetDimensions(int w, int h)
{
	m_size.w = w;
	m_size.h = h;
}

void ui::CPanel::SetPos(int x, int y)
{
	m_pos.x = x;
	m_pos.y = y;
}

void ui::CPanel::SetMovable(bool movable)
{
	m_movable = movable;
	UpdateFlags();
}

void ui::CPanel::SetResizable(bool resizable)
{
	m_resizable = resizable;
	UpdateFlags();
}

void ui::CPanel::SetClosable(bool closable)
{
	m_closable = closable;
	UpdateFlags();
}

void ui::CPanel::AddLayout(CLayout &widget)
{
	m_widgets.push_back(&widget);
}

void ui::CPanel::RemoveLayout(CLayout &widget)
{
	m_widgets.remove(&widget);
}

void ui::CPanel::Draw(void* drawData)
{
	if(!m_active)
		return;
	nk_context* pCtx = (nk_context*)drawData;
	nk_flags flags = m_flags;

	/* Update to latest fullscreen params */
	if(m_fullscreen)
	{
		m_size.w = ScreenWidth();
		m_size.h = ScreenWidth();
		m_pos.x = m_pos.y = 0;
	}

	nk_begin_titled(pCtx, m_name, m_title, nk_rect(m_pos.x, m_pos.y, m_size.w, m_size.h), flags);

	auto rect = nk_window_get_bounds(pCtx);
	m_size.w = rect.w;
	m_size.h = rect.h;
	m_pos.x = rect.x;
	m_pos.y = rect.y;

	for(auto& widget : m_widgets)
	{
		widget->Draw(drawData);
	}

	nk_end(pCtx);
}

void CPanel::UpdateFlags()
{
	m_flags = 0;
	if(m_closable)
		m_flags |= NK_WINDOW_CLOSABLE;
	if(m_movable)
		m_flags |= NK_WINDOW_MOVABLE;
	if(m_resizable)
		m_flags |= NK_WINDOW_SCALABLE;
	if(m_minimizable)
		m_flags |= NK_WINDOW_MINIMIZABLE;
	if(m_bordered)
		m_flags |= NK_WINDOW_BORDER;
	if(!m_scrollable)
		m_flags |= NK_WINDOW_NO_SCROLLBAR;
}

void CPanel::SetScrollable(bool scrollable)
{
	m_scrollable = scrollable;
	UpdateFlags();
}

void CPanel::SetMinimizable(bool minimizable)
{
	m_minimizable = minimizable;
	UpdateFlags();
}

void CPanel::SetBordered(bool bordered)
{
	m_bordered = bordered;
	UpdateFlags();
}
