
#include "Scheme.h"

using namespace ui;

static const char* gStringToColorTable[] = {
	"text", // COLOR_TEXT = 0,
	"window",	// COLOR_WINDOW,
	"header",	// COLOR_HEADER,
	"border",	// COLOR_BORDER,
	"button",	// COLOR_BUTTON,
	"button_hover",	// COLOR_BUTTON_HOVER,
	"button_active",	// COLOR_BUTTON_ACTIVE,
	"toggle",	// COLOR_TOGGLE,
	"toggle_hover",	// COLOR_TOGGLE_HOVER,
	"toggle_cursor",	// COLOR_TOGGLE_CURSOR,
	"select",	// COLOR_SELECT,
	"select_active",	// COLOR_SELECT_ACTIVE,
	"slider",	// COLOR_SLIDER,
	"slider_cursor",	// COLOR_SLIDER_CURSOR,
	"slider_cursor_hover",	// COLOR_SLIDER_CURSOR_HOVER,
	"slider_cursor_active",	// COLOR_SLIDER_CURSOR_ACTIVE,
	"slider_property",	// COLOR_PROPERTY,
	"edit",	// COLOR_EDIT,
	"edit_cursor",	// COLOR_EDIT_CURSOR,
	"combo",	// COLOR_COMBO,
	"chart",	// COLOR_CHART,
	"chart_color",	// COLOR_CHART_COLOR,
	"chart_color_highlight",	// COLOR_CHART_COLOR_HIGHLIGHT,
	"scrollbar",	// COLOR_SCROLLBAR,
	"scrollbar_cursor",	// COLOR_SCROLLBAR_CURSOR,
	"scrollbar_cursor_hover",	// COLOR_SCROLLBAR_CURSOR_HOVER,
	"scrollbar_cursor_active",	// COLOR_SCROLLBAR_CURSOR_ACTIVE,
	"tab_header",	// COLOR_TAB_HEADER,
};

ui::CScheme::CScheme()
{

}

ui::CScheme::~CScheme()
{

}

CScheme *ui::CScheme::FromKeyValues(KeyValues *pKV)
{
	CScheme* scheme = new CScheme();
	scheme->m_keyValues = *pKV;
	scheme->m_colorsSection = scheme->m_keyValues.GetChild("colors");

	return scheme;
}

CScheme *ui::CScheme::FromFile(const char *file)
{
	KeyValues* pKV = new KeyValues();
	pKV->ParseFile(file);
	return CScheme::FromKeyValues(pKV);
}

void ui::CScheme::Destroy()
{
	delete this;
}

static Color ParseColorString(const char* s)
{
	auto colo = strtoul(s, NULL, 16);
	Color c;
	c.Unpack(colo);
	return c;
}

ui::Color ui::CScheme::GetNamedColor(const char *name)
{
	if(!m_colorsSection)
		return Color::Black();
	const char* c = m_colorsSection->GetString(name);
	if(!c)
		return Color::Black();
	return ParseColorString(c);
}

void ui::CScheme::SetNamedColor(const char *name, Color color)
{
	if(!m_colorsSection)
		return;
	char c[64];
	snprintf(c, sizeof(c), "%u", color.Pack());
	m_colorsSection->SetString(name, c);
}

ui::Color ui::CScheme::GetElementColor(ESchemeColor color)
{
	if(color < NUM_COLORS && color >= 0)
		return m_colorTable[color];
	return Color(0,0,0,0);
}

void ui::CScheme::SetElementColor(ESchemeColor color, Color color1)
{
	if(color < NUM_COLORS && color >= 0)
	{
		m_colorTable[color] = color1;
	}
}

const KeyValues &ui::CScheme::GetKeyValues() const
{
	return m_keyValues;
}

::KeyValues &ui::CScheme::GetKeyValues()
{
	return m_keyValues;
}

CScheme *CScheme::Empty()
{
	return new CScheme();
}
