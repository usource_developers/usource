
#include "WidgetGroup.h"
#include "Widget.h"
#include "NkBackend.h"

using namespace ui;

ui::CWidgetGroup::CWidgetGroup(ui::IRenderable *parent, const char* name, const char *title) :
	CLayout(parent),
	m_title(title),
	m_name(name)
{

}

ui::CWidgetGroup::~CWidgetGroup()
{

}

void ui::CWidgetGroup::Draw(void *drawData)
{
	nk_context* pCtx = (nk_context*)drawData;
	if(m_active)
	{
		if(m_scrollable)
		{
			nk_scroll scroll;
			nk_group_scrolled_begin(pCtx, &scroll, m_title.c_str(), 0);
		}
		else
			nk_group_begin_titled(pCtx, m_name.c_str(), m_title.c_str(), 0);
		for(auto widget : m_widgets)
		{
			if(widget->Active())
				widget->Draw(drawData);
		}
		nk_group_end(pCtx);
	}
}
