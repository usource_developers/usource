
#pragma once

#include "MainUI2.h"
#include "Renderable.h"
#include "containers/list.h"

BEGIN_MAINUI_NAMESPACE

class CPanel;
class CWidget;

class CLayout : public ui::IRenderable
{
protected:
	List<ui::IRenderable*> m_widgets;
	CPanel* m_window;
public:
	CLayout(IRenderable* parent);
	virtual ~CLayout();

	const IRenderable* GetParent() const { return m_parent; };

	void AddWidget(CWidget& widget);
	void AddChild(IRenderable& renderable);

	virtual void Draw(void* drawData) = 0;

	/* m_window should always be valid! */
	CPanel& Window() { return *m_window; };
};

END_MAINUI_NAMESPACE
