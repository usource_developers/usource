
#pragma once

#include "MainUI2.h"
#include "Layout.h"
#include "Point.h"

BEGIN_MAINUI_NAMESPACE

class CGridLayout : public CLayout
{
protected:
	Size m_size;
	int m_cols;
public:
	CGridLayout(IRenderable* parent, int cols = 1, int item_height = 0, int item_wide = 0);
	virtual ~CGridLayout();

	int Width();
	int Height();
	Size GetSize();

	/**
	 * @param cols Sets the number of columns for the widgets
	 */
	void SetColumns(int cols);

	/**
	 * @return Returns the number of columns for the widgets
	 */
	int GetColumns();

	/**
	 * Sets the width of the layout to the value in PIXELS
	 * If 0, layout will fit to it's windows width
	 * @param w
	 */
	void SetWidth(int w);

	/**
	 * Sets the height of each item to the value in PIXELS
	 * IF set to 0, autolayouting will be used, and the height of the widget will be automatically
	 * determined
	 * @param h
	 */
	void SetHeight(int h);

	/**
	 * Sets the size of the widget
	 * if height is 0, autolayouting will be used.
	 * if width is 0, width will be set to the parent's window width
	 * @param point
	 */
	void SetSize(Size point);

	void Draw(void* drawData) override;
};

END_MAINUI_NAMESPACE
