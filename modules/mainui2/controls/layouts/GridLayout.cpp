#include "GridLayout.h"
#include "NkBackend.h"

using namespace ui;

CGridLayout::CGridLayout(IRenderable *parent, int cols, int item_height, int item_wide) :
	CLayout(parent),
	m_size(item_wide, item_height),
	m_cols(cols)
{

}

CGridLayout::~CGridLayout()
{

}

int CGridLayout::Width()
{
	return m_size.w;
}

int CGridLayout::Height()
{
	return m_size.h;
}

void CGridLayout::SetWidth(int w)
{
	m_size.w = w;
}

void CGridLayout::SetHeight(int h)
{
	m_size.h = h;
}

void CGridLayout::SetSize(Size point)
{
	m_size = point;
}

void CGridLayout::Draw(void *drawData)
{
	nk_context* pCtx = (nk_context*)drawData;

	if(m_size.w == 0)
	{
		nk_layout_row_dynamic(pCtx, m_size.h, m_cols);
	}
	else
	{
		nk_layout_row_static(pCtx, m_size.h, m_size.w, m_cols);
	}
	for(auto widget : m_widgets)
	{
		if(widget->Active())
			widget->Draw(pCtx);
	}
}
Size CGridLayout::GetSize() { return m_size; }
void CGridLayout::SetColumns(int cols)
{
	m_cols = cols;
}

int  CGridLayout::GetColumns()
{
	return m_cols;
}
