#include "Layout.h"
#include "Widget.h"
#include "Panel.h"

using namespace ui;


CLayout::~CLayout()
{

}

void CLayout::AddWidget(CWidget &widget)
{
	m_widgets.push_back(&widget);
	widget.m_parent = this;
}

void CLayout::AddChild(IRenderable &renderable)
{
	m_widgets.push_back(&renderable);
	renderable.m_parent = this;
}

CLayout::CLayout(IRenderable *parent) :
	ui::IRenderable(parent)
{
	/* *slight* hack here to find the root window */
	/* This is needed for some widgets later down the line */
	CPanel* frame = dynamic_cast<CPanel*>(parent);
	if(frame)
	{
		m_window = frame;
		return;
	}

	CLayout* layout = dynamic_cast<CLayout*>(parent);
	if(layout)
	{
		m_window = layout->m_window;
	}
}
