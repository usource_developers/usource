

#pragma once

#include "Layout.h"
#include "containers/string.h"

BEGIN_MAINUI_NAMESPACE

class CWidgetGroup : public CLayout
{
protected:
	bool m_scrollable;
	String m_title;
	String m_name;

	CWidgetGroup();
public:
	CWidgetGroup(IRenderable* parent, const char* name, const char* title = "");
	virtual ~CWidgetGroup();

	const char* Title() const { return m_title.c_str(); };
	bool Scrollable() const { return m_scrollable; };

	void SetTitle(const char* tit) { m_title = tit; };
	void SetScrollable(bool s) { m_scrollable = s; };

	void Draw(void* drawData) override;
};

END_MAINUI_NAMESPACE