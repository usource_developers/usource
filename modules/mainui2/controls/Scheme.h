#pragma once

#include "MainUI2.h"
#include "Color.h"

#include "keyvalues.h"

BEGIN_MAINUI_NAMESPACE

typedef enum
{
	COLOR_TEXT = 0,
	COLOR_WINDOW,
	COLOR_HEADER,
	COLOR_BORDER,
	COLOR_BUTTON,
	COLOR_BUTTON_HOVER,
	COLOR_BUTTON_ACTIVE,
	COLOR_TOGGLE,
	COLOR_TOGGLE_HOVER,
	COLOR_TOGGLE_CURSOR,
	COLOR_SELECT,
	COLOR_SELECT_ACTIVE,
	COLOR_SLIDER,
	COLOR_SLIDER_CURSOR,
	COLOR_SLIDER_CURSOR_HOVER,
	COLOR_SLIDER_CURSOR_ACTIVE,
	COLOR_PROPERTY,
	COLOR_EDIT,
	COLOR_EDIT_CURSOR,
	COLOR_COMBO,
	COLOR_CHART,
	COLOR_CHART_COLOR,
	COLOR_CHART_COLOR_HIGHLIGHT,
	COLOR_SCROLLBAR,
	COLOR_SCROLLBAR_CURSOR,
	COLOR_SCROLLBAR_CURSOR_HOVER,
	COLOR_SCROLLBAR_CURSOR_ACTIVE,
	COLOR_TAB_HEADER,
	NUM_COLORS
} ESchemeColor;

class CScheme
{
private:
	Color m_colorTable[NUM_COLORS];
	KeyValues m_keyValues;
	KeyValues* m_colorsSection = nullptr;
	CScheme();
public:
	~CScheme();

	static CScheme* Empty();
	static CScheme* FromKeyValues(KeyValues* pKV);
	static CScheme* FromFile(const char* file);
	void Destroy();

	Color GetNamedColor(const char* name);
	void SetNamedColor(const char* name, Color color);

	Color GetElementColor(ESchemeColor color);
	void SetElementColor(ESchemeColor color, Color color1);

	const KeyValues& GetKeyValues() const;
	KeyValues& GetKeyValues();
};

END_MAINUI_NAMESPACE