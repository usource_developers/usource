
#pragma once

#include "MainUI2.h"

BEGIN_MAINUI_NAMESPACE

class CLayout;
class CWidget;

class IRenderable
{
protected:
	bool m_active = true;
	IRenderable* m_parent;
	friend CLayout;
	friend CWidget;
	IRenderable() = delete;
public:
	/* You can actually fuck right off you fucking dickhead */
	/* Sick of these worthless copy constructors */
	IRenderable(IRenderable&) = delete;
	IRenderable(IRenderable&&) = delete;

	explicit IRenderable(IRenderable* parent) :
		m_parent(parent)
	{

	}

	virtual void Draw(void* drawData) = 0;

	bool Active() const { return m_active; };
	void SetActive(bool active) { m_active = active; };

	IRenderable* GetParent() { return m_parent; };

	bool IsPanel() const;
	bool IsWidget() const;
	bool IsLayout() const;
};

END_MAINUI_NAMESPACE
