
#pragma once

#include "MainUI2.h"
#include "Color.h"

#ifdef HAVE_NUKLEAR
#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#include "nuklear.h"
#endif

BEGIN_MAINUI_NAMESPACE

void NkInit();
void NkFrame();
void NkShutdown();
void NkVidInit();
void NkPrecache();

void NkMouseMoveEvent(int x, int y);
void NkKeyEvent(int key, bool down);
void NkCharEvent(int key);

inline nk_color ColorToNkColor(Color c)
{
	nk_color colo;
	colo.r = c.r;
	colo.g = c.g;
	colo.b = c.b;
	colo.a = c.a;
	return colo;
}

inline nk_style_item ColorToStyleItem(Color c)
{
	nk_style_item d;
	d.data.color = ColorToNkColor(c);
	d.type = NK_STYLE_ITEM_COLOR;
	return d;
}

END_MAINUI_NAMESPACE