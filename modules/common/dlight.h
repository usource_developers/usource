/***
 *
 *	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
 *
 *	This product contains software technology licensed from Id
 *	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
 *	All Rights Reserved.
 *
 *   Use, distribution, and modification of this source code and/or resulting
 *   object code is restricted to non-commercial enhancements to products from
 *   Valve LLC.  All other use, distribution, or modification is prohibited
 *   without written permission from Valve LLC.
 *
 ****/

#ifndef DLIGHT_H
#define DLIGHT_H

#define STYLE_STRING_LENGTH 64

typedef struct dlight_s
{
	vec3_t	 origin;
	float	 radius;
	color24	 color;
	float	 die;	   // stop lighting after this time
	float	 decay;	   // drop this each second
	float	 minlight; // don't add when contributing less
	int	 key;
	qboolean dark; // subtracts light instead of adding
} dlight_t;

enum class ELightFalloff
{
	CONST,
	LINEAR,
	QUADRATIC
};

/* Directional light */
typedef struct ddlight_s
{
	vec3_t origin;
	color24 color;
	vec3_t direction;
	float inner_ang;
	float outer_ang;
	float focus;
	ELightFalloff falloff_mode;
	char style[STYLE_STRING_LENGTH];
} ddlight_t;

#endif // DLIGHT_H
