/*
iassetsystem.h - A global assetsystem for the game and related tools
Copyright (C) 2021 Jeremy Lorelli

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/

#pragma once

#include <stddef.h>

#include "../searchpaths.h"
#include "appframework.h"

enum class EAssetType
{
	ASSET_UNKNOWN = 0,
	ASSET_SOUND,
	ASSET_MODEL,
	ASSET_TEXTURE,
	ASSET_MAP,
	ASSET_GENERICTEXT,
	ASSET_GENERICBINARY,
	ASSET_RESERVED0,
	ASSET_RESERVED1,
	ASSET_RESERVED2,
	ASSET_RESERVED3,
	ASSET_RESERVED4,
	ASSET_RESERVED5,
	ASSET_RESERVED6,
	ASSET_RESERVED7,
	ASSET_RESERVED8,
	ASSET_MAX
};

using AssetIndex = unsigned long long;

class IAssetHandle
{
public:
	/**
	 * @brief Get the asset type
	 *
	 * @return EAssetType
	 */
	virtual EAssetType GetType() const = 0;

	/**
	 * @brief Get the reference count
	 *
	 * @return size_t
	 */
	virtual size_t GetReferenceCount() const = 0;

	/**
	 * @brief Decrements the reference count
	 *
	 */
	virtual void DecrementRefCount() = 0;

	/**
	 * @brief Manually increment the reference count of the asset
	 *
	 * @return true Asset was valid and ref count was incremented
	 */
	virtual void IncrementRefCount() = 0;

	/**
	 * @brief Get the internal index of the asset
	 *
	 * @return AssetIndex
	 */
	virtual AssetIndex GetIndex() const = 0;

	/**
	 * @brief Forcibly disposes this object
	 *
	 */
	virtual void ForceDispose() = 0;

	/**
	 * @brief Attempts to dispose the asset.
	 *
	 * @return true Asset had no users and was disposed
	 */
	virtual bool TryDispose() = 0;

	/**
	 * @brief Returns if this asset is valid
	 *
	 * @return true Asset is valid and can be used
	 * @return false Assert is not valid and cannot be used
	 */
	virtual bool IsValid() = 0;

	/**
	 * @brief Invalidates the asset so nothing can use it
	 */
	virtual void Invalidate() = 0;

	/**
	 * @brief Get the memory usage of the object
	 *
	 * @return size_t Memory usage (in bytes)
	 */
	virtual size_t GetMemoryUsage() = 0;

	/**
	 * @brief Get the Internal data of the asset
	 *
	 * @return void*
	 */
	virtual void* GetInternalData() = 0;

	/**
	 * @brief Precaches the object in prep for use
	 */
	virtual void PreCache() = 0;

	/**
	 * @brief Clears precache data for the asset
	 */
	virtual void UnCache() = 0;
};

class IAssetSystem : public IAppInterface
{
public:
	/**
	 * @brief Get the Memory Usage of all assets used by the assetsystem
	 *
	 * @return size_t
	 */
	virtual size_t GetMemoryUsage() = 0;

	/**
	 * @brief Get the mem budget of the entire assetsystem
	 *
	 * @return size_t
	 */
	virtual size_t GetMemoryBudget() = 0;

	/**
	 * @brief Sets the memory budget of the assetsystem
	 *
	 * @param budget Memory budget (in bytes)
	 */
	virtual void SetMemoryBudget(size_t budget) = 0;

	/**
	 * @brief Forcibly run the assetsystem garbage collector to purge unused assets
	 */
	virtual void CollectGarbage() = 0;

	/**
	 * @brief Enable or disable automated garbage collection when the memory budget is approached
	 *
	 * @param enabled
	 */
	virtual void SetGarbageCollectorEnabled(bool enabled) = 0;

	/**
	 * @brief Load sound asset
	 *
	 * @param path
	 * @param search
	 * @return IAssetHandle*
	 */
	virtual IAssetHandle* LoadSound(const char* path, ESearchPath search = ESearchPath::ALL) = 0;

	/**
	 * @brief Load model asset
	 *
	 * @param path
	 * @param serch
	 * @return IAssetHandle*
	 */
	virtual IAssetHandle* LoadModel(const char* path, ESearchPath serch = ESearchPath::ALL) = 0;

	/**
	 * @brief Load map asset
	 *
	 * @param path
	 * @param search
	 * @return IAssetHandle*
	 */
	virtual IAssetHandle* LoadMap(const char* path, ESearchPath search = ESearchPath::ALL) = 0;

	/**
	 * @brief Load texture asset
	 *
	 * @param path
	 * @param search
	 * @return IAssetHandle*
	 */
	virtual IAssetHandle* LoadTexture(const char* path, ESearchPath search = ESearchPath::ALL) = 0;

	/**
	 * @brief Load generic text document asset. Generally you should just use normal vfs functions for this, but in the event you want to share
	 * 		text documents globally (or load a large volume of them), you can use this
	 *
	 * @param path
	 * @param search
	 * @return IAssetHandle*
	 */
	virtual IAssetHandle* LoadText(const char* path, ESearchPath search = ESearchPath::ALL) = 0;

	/**
	 * @brief Load generic binary data. Use this if none of the other LoadXXX functions correspond to what you want to load
	 *
	 * @param path
	 * @param search
	 * @return IAssetHandle*
	 */
	virtual IAssetHandle* LoadBinary(const char* path, ESearchPath search = ESearchPath::ALL) = 0;
};
