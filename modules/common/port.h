/*
port.h -- Portability Layer for Windows types
Copyright (C) 2015 Alibek Omarov

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/

#pragma once
#ifndef PORT_H
#define PORT_H

#if !defined(_WIN32)
#include <dlfcn.h>
#include <unistd.h>

#define PATH_SPLITTER "/"

#if defined(__APPLE__)
#include <sys/syslimits.h>
#include "TargetConditionals.h"
#define OS_LIB_EXT   "dylib"
#define OPEN_COMMAND "open"
#define OS_EXE_EXT   ""
#else
#define OS_LIB_EXT   "so"
#define OS_EXE_EXT   ""
#define OPEN_COMMAND "xdg-open"
#endif

#define OS_LIB_PREFIX "lib"

#if defined(__ANDROID__)
//#if defined(LOAD_HARDFP)
//	#define POSTFIX "_hardfp"
//#else
#define POSTFIX
//#endif
#else
#endif

#define VGUI_SUPPORT_DLL "libvgui_support." OS_LIB_EXT

// Windows-specific
#define __cdecl
#define __stdcall

#define _inline static inline
#define FORCEINLINE inline __attribute__((always_inline))
#define O_BINARY    0 // O_BINARY is Windows extension
#define O_TEXT	    0 // O_TEXT is Windows extension

// Windows functions to Linux equivalent
#define _mkdir(x)	       mkdir(x, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)
#define LoadLibrary(x)	       dlopen(x, RTLD_NOW)
#define GetProcAddress(x, y)   dlsym(x, y)
#define SetCurrentDirectory(x) (!chdir(x))
#define FreeLibrary(x)	       dlclose(x)
//#define MAKEWORD( a, b )			((short int)(((unsigned char)(a))|(((short int)((unsigned char)(b)))<<8)))
//#define max( a, b )                 (((a) > (b)) ? (a) : (b))
//#define min( a, b )                 (((a) < (b)) ? (a) : (b))
#define tell(a) lseek(a, 0, SEEK_CUR)

typedef unsigned char BYTE;
typedef short int     WORD;
typedef unsigned int  DWORD;
typedef int	      LONG;
typedef unsigned int  ULONG;
typedef int	      WPARAM;
typedef unsigned int  LPARAM;

typedef void* HANDLE;
typedef void* HMODULE;
typedef void* HINSTANCE;

typedef char* LPSTR;

typedef struct tagPOINT
{
	int x, y;
} POINT;
#else // WIN32
#include <io.h>
#define PATH_SPLITTER "\\"
#ifdef __MINGW32__
#undef _inline
#undef FORCEINLINE
#define _inline static inline
#define FORCEINLINE inline __attribute__((always_inline))
#else
#undef FORCEINLINE
#define FORCEINLINE __forceinline
#endif

#undef open
#undef read
#undef alloca
#define open   _open
#define read   _read
#define alloca _alloca

// shut-up compiler warnings
#pragma warning(disable : 4244) // MIPS
#pragma warning(disable : 4018) // signed/unsigned mismatch
#pragma warning(disable : 4305) // truncation from const double to float
#pragma warning(disable : 4115) // named type definition in parentheses
#pragma warning(disable : 4100) // unreferenced formal parameter
#pragma warning(disable : 4127) // conditional expression is constant
#pragma warning(disable : 4057) // differs in indirection to slightly different base types
#pragma warning(disable : 4201) // nonstandard extension used
#pragma warning(disable : 4706) // assignment within conditional expression
#pragma warning(disable : 4054) // type cast' : from function pointer
#pragma warning(disable : 4310) // cast truncates constant value

#define HSPRITE WINAPI_HSPRITE
#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <windows.h>
#undef HSPRITE

#define OS_LIB_PREFIX	 ""
#define OS_LIB_EXT	 "dll"
#define OS_EXE_EXT	 "exe"
#define VGUI_SUPPORT_DLL "../vgui_support." OS_LIB_EXT
#ifdef XASH_64BIT
// windows NameForFunction not implemented yet
#define XASH_ALLOW_SAVERESTORE_OFFSETS
#endif
#endif // WIN32

#include <stdlib.h>
#include <string.h>
#include <limits.h>

#if defined XASH_SDL && !defined REF_DLL
#include <algorithm>
#include <SDL2/SDL.h>
#endif

// Builds a library or exe name from the specified string literal
#define MAKE_LIB_NAME(x) OS_LIB_PREFIX x "." OS_LIB_EXT
#ifdef _WIN32
#define MAKE_EXE_NAME(x) x "." OS_EXE_EXT
#else
#define MAKE_EXE_NAME(x) x
#endif

//==========================================================//
// Feature test macros
//==========================================================//
#if ( defined(__AVX__) ) && !( defined(FORBID_SIMD) || defined(FORBID_AVX) )
#	undef USE_AVX
#	define USE_AVX 1
#endif
#if ( defined(__AVX2__) ) && !( defined(FORBID_SIMD) || defined(FORBID_AVX2) )
#	undef USE_AVX2
#	define USE_AVX2 1
#endif
#if ( defined(__SSE__) || defined(_M_X64) || (_M_IX86_FP >= 1) ) &&  !( defined(FORBID_SIMD) || defined(FORBID_SSE) )
#	undef USE_SSE
#	define USE_SSE 1
#endif
#if ( defined(__SSE2__) || defined(_M_X64) || (_M_IX86_FP >= 2) ) &&  !( defined(FORBID_SIMD) || defined(FORBID_SSE2) )
#	undef USE_SSE2
#	define USE_SSE2 1
#endif
#if ( defined(__SSE3__) || defined(_M_X64) ) &&  !( defined(FORBID_SIMD) || defined(FORBID_SSE3) )
#	undef USE_SSE3
#	define USE_SSE3 1
#endif
#if ( defined(__SSE4_1__) ) &&  !( defined(FORBID_SIMD) || defined(FORBID_SSE41) )
#	undef USE_SSE41
#	define USE_SSE41 1
#endif
#if ( defined(__SSE4_2__) ) &&  !( defined(FORBID_SIMD) || defined(FORBID_SSE42) )
#	undef USE_SSE42
#	define USE_SSE42 1
#endif
#if ( defined(__SSSE3__) ) &&  !( defined(FORBID_SIMD) || defined(FORBID_SSSE3) )
#	undef USE_SSSE3
#	define USE_SSSE3 1
#endif
#if ( defined(__ARM_NEON) ) && !( defined(FORBID_SIMD) || defined(FORBID_NEON) )
#	undef USE_NEON
#	define USE_NEON 1
#endif
#if ( defined(__ARM_FEATURE_SVE) ) && !( defined(FORBID_SIMD) || defined(FORBID_SVE) )
#	undef USE_SVE
#	define USE_SVE 1
#endif
#if ( defined(__ARM_FEATURE_SVE2) ) && !( defined(FORBID_SIMD) || defined(FORBID_SVE2) )
#	undef USE_SVE2
#	define USE_SVE2 1
#endif
#if ( defined(__ARM_FEATURE_CRC32) )
#	undef HAS_ARM_CRC32
#	define HAS_ARM_CRC32 1
#endif
#if ( defined(__ARM_FEATURE_FMA) )
#	undef HAS_ARM_FMA
#	define HAS_ARM_FMA 1
#endif
#if ( defined(__ARM_FEATURE_COMPLEX) )
#	undef HAS_ARM_COMPLEX
#	define HAS_ARM_COMPLEX 1
#endif
#if ( defined(__ARM_FEATURE_SVE) )
#	undef HAS_ARM_SVE
#	define HAS_ARM_SVE 1
#endif
#if ( defined(__ARM_FEATURE_SVE2) )
#	undef HAS_ARM_SVE2
#	define HAS_ARM_SVE2 1
#endif
#if ( defined(__amd64__) || defined(_M_X64_) )
#	undef PLATFORM_64BITS
#	undef _X64_
#	undef PLATFORM_AMD64
#	define PLATFORM_64BITS 1
#	define _X64_
#	define PLATFORM_AMD64 1
#endif
#if ( defined(__i386__) || defined(_M_IX86_) )
#	undef PLATFORM_32BITS
#	undef PLATFORM_X86
#	undef _X86_
#	define PLATFORM_32BITS 1
#	define PLATFORM_X86 1
#	define _X86_ 1
#endif
#if ( defined(_M_ARM) || defined(__arm__) )
#	undef PLATFORM_32BITS
#	undef PLATFORM_ARM
#	undef PLATFORM_ARM32
#	define PLATFORM_32BITS 1
#	define PLATFORM_ARM 1
#	define PLATFORM_ARM32 1
#endif
#if ( defined(_M_ARM64) || defined(__aarch64__) )
#	undef PLATFORM_64BITS
#	undef PLATFORM_ARM
#	undef PLATFORM_ARM64
#	define PLATFORM_64BITS 1
#	define PLATFORM_ARM 1
#	define PLATFORM_ARM64 1
#endif
#if ( defined(__ppc64__) )
#	undef PLATFORM_64BITS
#	undef PLATFORM_PPC
#	undef PLATFORM_PPC64
#	define PLATFORM_64BITS 1
#	define PLATFORM_PPC 1
#	define PLATFORM_PPC64 1
#elif ( defined(__ppc__) )
#	undef PLATFORM_32BITS
#	undef PLATFORM_PPC
#	undef PLATFORM_PPC32
#	define PLATFORM_32BITS 1
#	define PLATFORM_PPC 1
#	define PLATFORM_PPC32 1
#endif
#if ( defined(__riscv) )
#	undef PLATFORM_64BITS
#	undef PLATFORM_RISCV
#	undef PLATFORM_RISCV64
#	define PLATFORM_64BITS 1
#	define PLATFORM_RISCV 1
#	define PLATFORM_RISCV64 1
#endif
#if ( defined(__GNUC__) )
#	undef COMPILER_GCC
#	define COMPILER_GCC 1
#endif
#if ( defined(_MSC_VER) )
#	undef COMPILER_MSVC
#	define COMPILER_MSVC 1
#endif
#if ( defined(__clang__) )
#	undef COMPILER_CLANG
#	define COMPILER_CLANG 1
#endif
#if ( defined(__linux__) )
#	undef OS_LINUX
#	define OS_LINUX 1
#endif
#if ( defined(_WIN32) )
#	undef OS_WINDOWS
#	define OS_WINDOWS 1
#endif
#if ( defined(__APPLE__) )
#	undef OS_OSX
#	define OS_OSX 1
#endif
#if ( defined(__ANDROID__) )
#	undef OS_ANDROID
#	define OS_ANDROID
#endif
#if ( __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__ )
#	undef PLATFORM_BIG_ENDIAN
#	define PLATFORM_BIG_ENDIAN 1
#else
#	undef PLATFORM_LITTLE_ENDIAN
#	define PLATFORM_LITTLE_ENDIAN 1
#endif


#endif // PORT_H
