// Image format enums
#pragma once

enum class EImageFormat
{
	D24S8,		// 24-bit depth, 8-bit stencil
	D32FS8,		// 32-bit float depth, 8-bit stencil
	D24,		// 24-bit depth
	D16,		// 16-bit depth
	D32F,		// 32-bit float depth
	RGB8,		// 8-bit per channel RGB
	RGBA8,		// 8-bit per channel RGBA
	RGB32I,		// 32-bit per channel RGB (integer)
	RGB32F,		// 32-bit float per channel RGB
	RGBA32I,	// 32-bit per channel RGBA (int)
	RGBA32F,	// 32-bit float per channel RGBA
	R8,		// 8-bit single channel R
	RGB16,		// 16-bit per channel RGB
	RGB16F,		// 16-bit float per channel RGB (usually used for HDR)
	RGBA16I,	// 16-bit per channel RGBA
	RGBA16F,	// 16-bit float per channel RGBA
	SRGBA8,		// 8-bit per channel sRGBA
	SRGB8,		// 8-bit per channel sRGB
	RGB10A2,	// 10-bit per channel RGB, 2-bit alpha
	RG8,		// 8-bit per channel RG
	R16,		// 16-bit per channel R (so only one channel)
	R16F,		// 16-bit float per channel R
	RG16,		// RG 16-bit per channel
	RG16F,		// RG 16-bit float per channel
	DXT1,		// DXT1 compressed
	DXT3,		// DXT3 compressed
	DXT5,		// DXT5 compressed
	ATI2N		// ATI2N compressed
};

enum class EImageFilter
{
	NEAREST,
	BILINEAR,
	TRILINIEAR
};
