#pragma once

#include "common/const.h"
#include "engine/eiface.h"
#include "public/appframework.h"

class CPhysicsEntityData;

#define PHYSICS_INTERFACE_001 "IPhysics001"
#define PHYSICS_INTERFACE PHYSICS_INTERFACE_001

class IPhysicsEnvironment;

class IPhysicsInterface : public IAppInterface
{
public:
	virtual IPhysicsEnvironment* CreateEnvironment(const char* debugname) = 0;

	virtual void DestroyEnvironment(IPhysicsEnvironment* env) = 0;

	virtual IPhysicsEnvironment* GetDefaultEnvironment() = 0;

	virtual void Frame(float dt) = 0;

	// passed through pfnCreate (0 is attempt to create, -1 is reject)
	virtual int CreateEntity(edict_t* pent, const char* szName) = 0;

	// run custom physics for each entity (return 0 to use built-in engine physic)
	virtual int PhysicsEntity(edict_t* pEntity) = 0;

	// spawn entities with internal mod function e.g. for re-arrange spawn order (0 - use engine parser, 1 - use mod parser)
	virtual int LoadEntities(const char* mapname, char* entities) = 0;

	// update conveyor belt for clients
	virtual void UpdatePlayerBaseVelocity(edict_t* ent) = 0;

	// The game .dll should return 1 if save game should be allowed
	virtual int AllowSaveGame(void) = 0;

	// ONLY ADD NEW FUNCTIONS TO THE END OF THIS STRUCT.  INTERFACE VERSION IS FROZEN AT 6
	// override trigger area checking and touching
	virtual int TriggerTouch(edict_t* pent, edict_t* trigger) = 0;

	// some engine features can be enabled only through this function
	virtual unsigned int CheckFeatures(void) = 0;

	// used for draw debug collisions for custom physic engine etc
	virtual void DrawDebugTriangles(void) = 0;

	// used for draw debug overlay (textured)
	virtual void DrawNormalTriangles(void) = 0;

	// used for draw debug messages (2d mode)
	virtual void DrawOrthoTriangles(void) = 0;

	// tracing entities with SOLID_CUSTOM mode on a server (not used by pmove code)
	virtual void ClipMoveToEntity(edict_t* ent, const float* start, float* mins, float* maxs, const float* end, trace_t* trace) = 0;

	// tracing entities with SOLID_CUSTOM mode on a server (only used by pmove code)
	virtual void ClipPMoveToEntity(struct physent_s* pe, const float* start, float* mins, float* maxs, const float* end, struct pmtrace_s* tr) = 0;

	// obsolete
	virtual void PrepWorldFrame(void) = 0;

	// called through save\restore process
	virtual void CreateEntitiesInRestoreList(SAVERESTOREDATA* pSaveData, int levelMask, qboolean create_world) = 0;

	// helper for restore custom decals that have custom message (e.g. Paranoia)
	virtual int RestoreDecal(struct decallist_s* entry, edict_t* pEdict, qboolean adjacent) = 0;

	// handle custom trigger touching for player
	virtual void PlayerTouch(struct playermove_s* ppmove, edict_t* client) = 0;

	// alloc or destroy model custom data (called only for dedicated servers, otherwise using an client version)
	virtual void Mod_ProcessUserData(struct model_s* mod, qboolean create, const byte* buffer) = 0;

	// select BSP-hull for trace with specified mins\maxs
	virtual void* HullForBsp(edict_t* ent, const float* mins, const float* maxs, float* offset) = 0;

	// handle player custom think function
	virtual int PlayerThink(edict_t* ent, float frametime, double time) = 0;
};

class IPhysicsEnvironment
{
public:
	/**
	 * @brief Adds an entity to the environment
	 *
	 * @param ent Entity to add
	 * @param name name of ent
	 * @return true Added
	 * @return false Was not able to add
	 */
	virtual bool AddEntity(edict_t* ent, const char* name) = 0;

	/**
	 * @brief Remove entity from simulation
	 *
	 * @param ent Ent to remove
	 */
	virtual void RemoveEntity(edict_t* ent) = 0;

	/**
	 * @brief Run physics for the specific entity
	 *
	 * @param ent Entity to run physics for
	 * @return int Return 0 to fallback to built-in engine physics, 1 to run physics engine phys
	 */
	virtual int RunPhysics(edict_t* ent) = 0;

	/**
	 * @brief Simulate physics for the environment
	 *
	 * @param dt Frame time
	 */
	virtual void Simulate(float dt) = 0;

	/**
	 * @brief Sets the gravity for the environment
	 *
	 * @param gravity gravity vector
	 */
	virtual void SetGravity(Vector gravity) = 0;

	virtual Vector GetGravity() = 0;
};
