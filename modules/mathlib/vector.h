/*

VECTOR.H

Replacement for Valve's vector types.

*/
#pragma once

/* Redefs of stuff from good ol gold source */
typedef float vec_t;

#include <stdint.h>
#include <stdio.h>
#include <math.h>

#ifndef M_PI
#define M_PI 3.1415926535f
#endif

//========================================================================================================//
typedef unsigned int func_t;
typedef int	     string_t;
typedef float	     vec_t;
//========================================================================================================//

//========================================================================================================//
//=========================================================
// 2DVector - used for many pathfinding and many other
// operations that are treated as planar rather than 3d.
//=========================================================
class Vector2D
{
public:
	inline Vector2D(void) {}
	inline Vector2D(float X, float Y)
	{
		x = X;
		y = Y;
	}
	inline Vector2D operator+(const Vector2D& v) const { return Vector2D(x + v.x, y + v.y); }
	inline Vector2D operator-(const Vector2D& v) const { return Vector2D(x - v.x, y - v.y); }
	inline Vector2D operator*(float fl) const { return Vector2D(x * fl, y * fl); }
	inline Vector2D operator/(float fl) const { return Vector2D(x / fl, y / fl); }

	inline float Length(void) const { return (float)sqrt(x * x + y * y); }

	inline Vector2D Normalize(void) const
	{
		Vector2D vec2;

		float flLen = Length();
		if (flLen == 0)
		{
			return Vector2D((float)0, (float)0);
		}
		else
		{
			flLen = 1 / flLen;
			return Vector2D(x * flLen, y * flLen);
		}
	}

	vec_t x, y;
};

#undef DotProduct
#undef CrossProduct

//========================================================================================================//

inline float	DotProduct(const Vector2D& a, const Vector2D& b) { return (a.x * b.x + a.y * b.y); }
inline Vector2D operator*(float fl, const Vector2D& v) { return v * fl; }

//========================================================================================================//

//========================================================================================================//
//=========================================================
// 3D Vector
//=========================================================
class Vector // same data-layout as engine's vec3_t,
{	     // which is a vec_t[3]
public:
	// Construction/destruction
	inline Vector(void) {}
	inline Vector(float X, float Y, float Z)
	{
		x = X;
		y = Y;
		z = Z;
	}
	// inline Vector( double X, double Y, double Z )		{ x = (float)X; y = (float)Y; z = (float)Z; }
	// inline Vector( int X, int Y, int Z )			{ x = (float)X; y = (float)Y; z = (float)Z; }
	inline Vector(const Vector& v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
	}
	inline Vector(float rgfl[3])
	{
		x = rgfl[0];
		y = rgfl[1];
		z = rgfl[2];
	}

	// Operators
	inline Vector operator-(void) const { return Vector(-x, -y, -z); }
	inline int    operator==(const Vector& v) const { return x == v.x && y == v.y && z == v.z; }
	inline int    operator!=(const Vector& v) const { return !(*this == v); }
	inline Vector operator+(const Vector& v) const { return Vector(x + v.x, y + v.y, z + v.z); }
	inline Vector operator-(const Vector& v) const { return Vector(x - v.x, y - v.y, z - v.z); }
	inline Vector operator*(float fl) const { return Vector(x * fl, y * fl, z * fl); }
	inline Vector operator/(float fl) const { return Vector(x / fl, y / fl, z / fl); }

	// Methods
	inline void  CopyToArray(float* rgfl) const { rgfl[0] = x, rgfl[1] = y, rgfl[2] = z; }
	inline float Length(void) const { return (float)sqrt(x * x + y * y + z * z); }
		     operator float*() { return &x; }		  // Vectors will now automatically convert to float * when needed
		     operator const float*() const { return &x; } // Vectors will now automatically convert to float * when needed

	inline Vector Normalize(void) const
	{
		float flLen = Length();
		if (flLen == 0)
			return Vector(0, 0, 1); // ????
		flLen = 1 / flLen;
		return Vector(x * flLen, y * flLen, z * flLen);
	}

	inline Vector2D Make2D(void) const
	{
		Vector2D Vec2;

		Vec2.x = x;
		Vec2.y = y;

		return Vec2;
	}

	inline float Length2D(void) const { return (float)sqrt(x * x + y * y); }

	// Members
	vec_t x, y, z;

	Vector& operator=(const Vector& other)
	{
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
		return *this;
	}
};

//========================================================================================================//

#define VECTOR_COPY(a, b) ((b)[0] = (a)[0], (b)[1] = (a)[1], (b)[2] = (a)[2])

template <class X, class Y> float DotProduct(const X& x, const Y& y) { return ((x)[0] * (y)[0] + (x)[1] * (y)[1] + (x)[2] * (y)[2]); }

template <class A, class B, class C> inline void CrossProduct(const A& a, const B& b, C& c)
{
	((c)[0] = (a)[1] * (b)[2] - (a)[2] * (b)[1], (c)[1] = (a)[2] * (b)[0] - (a)[0] * (b)[2], (c)[2] = (a)[0] * (b)[1] - (a)[1] * (b)[0]);
}

template <class X, class Y> float DotProductAbs(const X& x, const Y& y)
{
	return (abs((x)[0] * (y)[0]) + abs((x)[1] * (y)[1]) + abs((x)[2] * (y)[2]));
}

template <class X, class Y> float DotProductFabs(const X& x, const Y& y)
{
	return (fabs((x)[0] * (y)[0]) + fabs((x)[1] * (y)[1]) + fabs((x)[2] * (y)[2]));
}

template <class A, class B, class C> void Vector2Subtract(const A& a, const B& b, C& c) { ((c)[0] = (a)[0] - (b)[0], (c)[1] = (a)[1] - (b)[1]); }

template <class A, class B, class C> void VectorSubtract(const A& a, const B& b, C& c)
{
	((c)[0] = (a)[0] - (b)[0], (c)[1] = (a)[1] - (b)[1], (c)[2] = (a)[2] - (b)[2]);
}

template <class A, class B, class C> void Vector2Add(const A& a, const B& b, C& c) { ((c)[0] = (a)[0] + (b)[0], (c)[1] = (a)[1] + (b)[1]); }

template <class A, class B, class C> void VectorAdd(const A& a, const B& b, C& c)
{
	((c)[0] = (a)[0] + (b)[0], (c)[1] = (a)[1] + (b)[1], (c)[2] = (a)[2] + (b)[2]);
}

template <class A, class B, class C> void VectorAddScalar(const A& a, const B& b, C& c)
{
	((c)[0] = (a)[0] + (b), (c)[1] = (a)[1] + (b), (c)[2] = (a)[2] + (b));
}

template <class A, class B> void Vector2Copy(const A& a, B& b) { ((b)[0] = (a)[0], (b)[1] = (a)[1]); }

template <class A, class B> void VectorCopy(const A& a, B& b) { ((b)[0] = (a)[0], (b)[1] = (a)[1], (b)[2] = (a)[2]); }

template <class A, class B> void Vector4Copy(const A& a, B& b) { ((b)[0] = (a)[0], (b)[1] = (a)[1], (b)[2] = (a)[2], (b)[3] = (a)[3]); }

template <class A, class B, class C> void VectorScale(const A& in, B scale, C& out)
{
	((out)[0] = (in)[0] * (scale), (out)[1] = (in)[1] * (scale), (out)[2] = (in)[2] * (scale));
}

template <class A, class B> bool VectorCompare(const A& v1, const B& v2) { return ((v1)[0] == (v2)[0] && (v1)[1] == (v2)[1] && (v1)[2] == (v2)[2]); }

template <class IN, class D, class OUT> void VectorDivide(const IN& in, const D& d, OUT& out) { VectorScale(in, (1.0f / (d)), out); }

template <class A> float VectorMax(const A& a) { return (max((a)[0], max((a)[1], (a)[2]))); }

template <class A> float VectorAvg(const A& a) { return (((a)[0] + (a)[1] + (a)[2]) / 3); }

template <class A> float VectorLength(const A& a) { return (sqrt(DotProduct(a, a))); }

template <class A> float VectorLength2(const A& a) { return (DotProduct(a, a)); }

template <class A, class B> float VectorDistance(const A& a, const B& b) { return (sqrt(VectorDistance2(a, b))); }

template <class A, class B> float VectorDistance2(const A& a, const B& b)
{
	return (((a)[0] - (b)[0]) * ((a)[0] - (b)[0]) + ((a)[1] - (b)[1]) * ((a)[1] - (b)[1]) + ((a)[2] - (b)[2]) * ((a)[2] - (b)[2]));
}

template <class A, class B, class O> void Vector2Average(const A& a, const B& b, O& o)
{
	((o)[0] = ((a)[0] + (b)[0]) * 0.5f, (o)[1] = ((a)[1] + (b)[1]) * 0.5f);
}

template <class A, class B, class O> void VectorAverage(const A& a, const B& b, O& o)
{
	((o)[0] = ((a)[0] + (b)[0]) * 0.5f, (o)[1] = ((a)[1] + (b)[1]) * 0.5f, (o)[2] = ((a)[2] + (b)[2]) * 0.5f);
}

template <class V, class X, class Y> void Vector2Set(V& v, X x, Y y) { ((v)[0] = (x), (v)[1] = (y)); }

template <class V, class X, class Y, class Z> void VectorSet(V& v, X x, Y y, Z z) { ((v)[0] = (x), (v)[1] = (y), (v)[2] = (z)); }

template <class V, class A, class B, class C, class D> void Vector4Set(V& v, A a, B b, C c, D d)
{
	((v)[0] = (a), (v)[1] = (b), (v)[2] = (c), (v)[3] = (d));
}

template <class X> void VectorClear(X& x) { ((x)[0] = (x)[1] = (x)[2] = 0); }

template <class V> bool VectorIsNAN(const V& v) { return (IS_NAN(v[0]) || IS_NAN(v[1]) || IS_NAN(v[2])); }

template <class V1, class L, class V2, class C> void Vector2Lerp(const V1& v1, L lerp, const V2& v2, C& c)
{
	((c)[0] = (v1)[0] + (lerp) * ((v2)[0] - (v1)[0]), (c)[1] = (v1)[1] + (lerp) * ((v2)[1] - (v1)[1]));
}

template <class V1, class L, class V2, class C> void VectorLerp(const V1& v1, L lerp, const V2& v2, C& c)
{
	((c)[0] = (v1)[0] + (lerp) * ((v2)[0] - (v1)[0]), (c)[1] = (v1)[1] + (lerp) * ((v2)[1] - (v1)[1]),
	 (c)[2] = (v1)[2] + (lerp) * ((v2)[2] - (v1)[2]));
}

template <class V> float VectorNormalize(V& v)
{
	float ilength = (float)sqrt(DotProduct(v, v));
	if (ilength)
		ilength = 1.0f / ilength;
	v[0] *= ilength;
	v[1] *= ilength;
	v[2] *= ilength;
	return ilength;
}

template <class V, class D> void VectorNormalize2(const V& v, D& dest)
{
	float ilength = (float)sqrt(DotProduct(v, v));
	if (ilength)
		ilength = 1.0f / ilength;
	dest[0] = v[0] * ilength;
	dest[1] = v[1] * ilength;
	dest[2] = v[2] * ilength;
}

template <class V> void VectorNormalizeFast(V& v)
{
	float ilength = (float)rsqrt(DotProduct(v, v));
	v[0] *= ilength;
	v[1] *= ilength;
	v[2] *= ilength;
}

template <class V> float VectorNormalizeLength(const V& v) { return VectorNormalizeLength2((v), (v)); }

template <class X, class Y> void VectorNegate(const X& x, Y& y) { ((y)[0] = -(x)[0], (y)[1] = -(x)[1], (y)[2] = -(x)[2]); }

template <class S, class B, class C> void VectorM(const S& scale1, const B& b1, C& c)
{
	((c)[0] = (scale1) * (b1)[0], (c)[1] = (scale1) * (b1)[1], (c)[2] = (scale1) * (b1)[2]);
}

template <class A, class S, class B, class C> void VectorMA(const A& a, S scale, const B& b, C& c)
{
	((c)[0] = (a)[0] + (scale) * (b)[0], (c)[1] = (a)[1] + (scale) * (b)[1], (c)[2] = (a)[2] + (scale) * (b)[2]);
}

template <class S, class B1, class S2, class B2, class C> void VectorMAM(const S& scale1, const B1& b1, const S2& scale2, const B2& b2, C& c)
{
	((c)[0] = (scale1) * (b1)[0] + (scale2) * (b2)[0], (c)[1] = (scale1) * (b1)[1] + (scale2) * (b2)[1],
	 (c)[2] = (scale1) * (b1)[2] + (scale2) * (b2)[2]);
}

template <class S1, class B1, class S2, class B2, class S3, class B3, class C>
void VectorMAMAM(const S1& scale1, const B1& b1, const S2& scale2, const B2& b2, const S3& scale3, const B3& b3, C& c)
{
	((c)[0] = (scale1) * (b1)[0] + (scale2) * (b2)[0] + (scale3) * (b3)[0], (c)[1] = (scale1) * (b1)[1] + (scale2) * (b2)[1] + (scale3) * (b3)[1],
	 (c)[2] = (scale1) * (b1)[2] + (scale2) * (b2)[2] + (scale3) * (b3)[2]);
}

template <class V> bool VectorIsNull(const V& v) { return ((v)[0] == 0.0f && (v)[1] == 0.0f && (v)[2] == 0.0f); }

template <class O, class X, class Y, class Z, class W> void MakeRGBA(O& out, X x, Y y, Z z, W w) { Vector4Set(out, x, y, z, w); }

template <class POINT, class PLANE> float PlaneDist(const POINT& point, const PLANE& plane)
{
	return ((plane)->type < 3 ? (point)[(plane)->type] : DotProduct((point), (plane)->normal));
}

template <class POINT, class PLANE> float PlaneDiff(const POINT& point, const PLANE& plane)
{
	return (((plane)->type < 3 ? (point)[(plane)->type] : DotProduct((point), (plane)->normal)) - (plane)->dist);
}

#undef bound
template <class MIN, class NUM, class MAX> auto bound(const MIN& min, const NUM& num, const MAX& max)
{
	return ((num) >= (min) ? ((num) < (max) ? (num) : (max)) : (min));
}

inline Vector operator*(float fl, const Vector& v) { return v * fl; }
inline float  DotProduct(const Vector& a, const Vector& b) { return (a.x * b.x + a.y * b.y + a.z * b.z); }
inline Vector CrossProduct(const Vector& a, const Vector& b) { return Vector(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x); }

namespace mathlib
{
template <class X, class Y> static float VectorDot(const X a, const Y b) { return (a[0] * b[0] + a[1] * b[1] + a[2] * b[2]); }

template <class X> static float VectorLength(const X x) { return (float)sqrtf(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]); }

template <class X> static float VectorNormalize(X& v)
{
	float ilength = (float)sqrt(DotProduct(v, v));
	if (ilength)
		ilength = 1.0f / ilength;
	v[0] *= ilength;
	v[1] *= ilength;
	v[2] *= ilength;
	return ilength;
}

template <class X, class Y> static float VectorDistance(X a, Y b)
{
	return ((a)[0] - (b)[0]) * ((a)[0] - (b)[0]) + ((a)[1] - (b)[1]) * ((a)[1] - (b)[1]) + ((a)[2] - (b)[2]) * ((a)[2] - (b)[2]);
}

template <class X, class Y> static float AngleBetweenVectors(const X v1, const Y v2)
{
	float angle;
	float l1 = mathlib::VectorLength(v1);
	float l2 = mathlib::VectorLength(v2);

	if (!l1 || !l2)
		return 0.0f;

	angle = acos(DotProduct(v1, v2) / (l1 * l2));
	angle = (angle * 180.0f) / M_PI;

	return angle;
}

template <class X> static void VectorInverse(X& v)
{
	v[0] = -v[0];
	v[1] = -v[1];
	v[2] = -v[2];
}
} // namespace mathlib

#if defined(CLIENT_DLL) || defined(SERVER_DLL)

/* Required for the game code */
#define vec3_t Vector
typedef vec_t vec4_t[4];
typedef vec_t vec2_t[2];

#else

/* Required for the engine and other module code */
typedef vec_t vec3_t[3];
typedef vec_t vec4_t[4];
typedef vec_t vec2_t[2];

#endif
