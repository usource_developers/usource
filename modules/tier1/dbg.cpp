#include "dbg.h"

#include <stdarg.h>
#include <stdlib.h>

static unsigned char g_color_red[3]    = {255, 0, 0};
static unsigned char g_color_yellow[3] = {255, 255, 0};

const char* _vpaste(const char* fmt, ...)
{
	static thread_local char tmpbuf[8192];

	va_list val;
	va_start(val, fmt);
	vsnprintf(tmpbuf, sizeof(tmpbuf), fmt, val);
	va_end(val);

	return tmpbuf;
}

void Msg(const char* fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	Log::Log(Log::GENERAL_CHANNEL_ID, ELogLevel::MSG_GENERAL, fmt, va);
	va_end(va);
}

void Warn(const char* fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	Log::Log(Log::GENERAL_CHANNEL_ID, ELogLevel::MSG_WARN, {255, 255, 0}, fmt, va);
	va_end(va);
}

void Error(const char* fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	Log::Log(Log::GENERAL_CHANNEL_ID, ELogLevel::MSG_ERROR, {255, 0, 0}, fmt, va);
	va_end(va);
}

void FatalError(const char* fmt, ...)
{
	char	tmp[8192];
	va_list va;
	va_start(va, fmt);
	vsnprintf(tmp, sizeof(tmp), fmt, va);
	va_end(va);
	g_pEngineDebug->HostError(tmp);

	Log::Log(Log::GENERAL_CHANNEL_ID, ELogLevel::MSG_FATAL, {255, 0, 0}, tmp);
}

void MsgC(LogColor color, const char* fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	Log::Log(Log::GENERAL_CHANNEL_ID, ELogLevel::MSG_GENERAL, color, fmt, va);
	va_end(va);
}
