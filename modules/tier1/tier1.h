/**
 *
 * tier1
 * 	Helper library for engine users
 *
 */
#pragma once

#include "engine_int.h"

extern IEngineCvar*  g_pEngineCvar;
extern IEngineDebug* g_pEngineDebug;

namespace tier1
{
void Connect();
}